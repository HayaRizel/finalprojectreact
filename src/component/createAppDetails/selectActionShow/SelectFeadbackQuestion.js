import React, { useState,useEffect } from 'react'
import { v4 as uuid } from 'uuid'
import ModalOneAction from './ModalOneAction'
import {  Button,  Select, Row,  Tooltip } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { deleteActionFromDb } from './addActionToDb';
import PropTypes from 'prop-types'
import {db_experiments} from '../../../services/firestore.config'



const feadbackKindsArr = [
    "question_open",
    "question_close",
    "question_range"
]
const actionLabelName = {
    "question_open": "open question",
    "question_close": "close question",
    "question_range": "range question",
}


function SelectFeadbackQuestion(props) {

    const [cardActionList, setcardActionList] = useState([]);
    const [selectedKind, setselectedKind] = useState();

    useEffect(() => {
        db_experiments.doc(props.expKey)
        .get()
        .then((doc) => {
            if (doc.exists) {
                let app_flow = doc.data().app_flow;
                //check if there is no app_flow or feadback and fix it if need to
                if(doc.data().app_flow == null || app_flow.find(x => x.flow_key === "feadback") == null){
                    const newFeadbackObj = {
                        flow_name: "feadback",
                        action_arr: [],
                        how_much_select: 1,
                        flow_key: "feadback"  
                    }
                    if(doc.data().app_flow != null){
                        app_flow = [...doc.data().app_flow,newFeadbackObj]
                    }else{
                        app_flow = [newFeadbackObj]
                    }
                    //add app_flow to db
                    db_experiments.doc(props.expKey)
                    .update({
                        app_flow: app_flow,
                    }, { merge: true })
                    .then(ret => {
                        console.log(ret);
                    })
                    .catch(err => {
                        console.error(err)
                    });            
                }
                const initialCardActionList = app_flow.find(x => x.flow_key === "feadback").action_arr.map(action => {
                    return {
                        key: action.action_id,
                        action_name: action.action_name,
                        initialValue: action.action_value
                    }
                })
                if(initialCardActionList == null){
                    initialCardActionList = [];
                }
                setcardActionList(initialCardActionList);
        
                return function cleanup() {
                }
            }
        })
        .catch((error) => {
            console.log("Error getting document:", error);
        });
    
    }, [])


    const delActionCard = (id) => {
        deleteActionFromDb(id,props.expKey)
        setcardActionList(cardActionList.filter(x => x.key !== id));
    }

    return (
        <div>
            <Row>
                <Select
                    placeholder="Select a Kind Of Action"
                    style={{ width: "150px" }}
                    onChange={(value) => { setselectedKind(value) }}
                >
                    {feadbackKindsArr.map(action_name => (
                        <Select.Option value={action_name} key={action_name}>{actionLabelName[action_name]}</Select.Option>
                    ))}
                </Select>
                <Tooltip title="add new feadback">
                    <Button icon={<PlusOutlined />}
                        onClick={() => {
                            if (selectedKind == null) return;
                            setcardActionList([{
                                key: uuid(),
                                action_name: selectedKind
                            }
                                , ...cardActionList])
                        }}>
                    </Button>
                </Tooltip>
            </Row>
            {cardActionList.map(cardAction => (
                <ModalOneAction 
                    action_name = {cardAction.action_name}
                    initialValue = {cardAction.initialValue}
                    key = {cardAction.key}
                    id = {cardAction.key}
                    flow_key = "feadback"
                    expKey = {props.expKey}
                    delActionCard = {delActionCard.bind(this, cardAction.key)}
                />
            ))}

        </div>
    )
}

SelectFeadbackQuestion.propTypes = {
    expKey: PropTypes.string,
    action_arr: PropTypes.array
}

export default SelectFeadbackQuestion

