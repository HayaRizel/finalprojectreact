import React, {useState,useEffect} from 'react'
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer, Bar, BarChart } from 'recharts';
import moment from 'moment';
import { Row,Col,Descriptions,Radio } from "antd";


function SpecificParticipantCloseQuestionChart(props) {
    let question_obj = props.question_obj;
    // close_question = {"action_name": "question_close",
    //     "action_id": props.id,
    //     "action_value": {
    //         "question_text": values.question_text,
    //         "answer" : {
    //             "show_feadback_option": true",
    //             "answers": {answer: "gfdsg", answer_id: "1"},
    //             "correct_answers" : ["1"]
    //             }
    //         }
    //     }
    const question_text = question_obj.action_value.question_text;
    const [answerList, setanswerList] = useState([])
    const [graphDataByDate, setgraphDataByDate] = useState([])
    const [graphDataBarCount, setgraphDataBarCount] = useState([])

    const [radioValue, setradioValue] = useState("graphDataBarCount")
    useEffect(() => {
        if(props.seizureListByParticipantDict == null || props.participantId == null || props.seizureListByParticipantDict[props.participantId] == null) return;
        
        const answerList = question_obj.action_value.answer.answers.map(ans => {
            return ans.answer
        });
        setanswerList(answerList);
        const answerCountArr = question_obj.action_value.answer.answers.map(ans => {return 0});

        const seizureList = props.seizureListByParticipantDict[props.participantId];

        //count graph initial
        seizureList.forEach(seizure => {
            if(seizure.data.feedback_question == null || seizure.data.feedback_question[question_text] == null) return;
            const answerIndex = answerList.indexOf(seizure.data.feedback_question[question_text]);
            answerCountArr[answerIndex]++;
        });
        let dataByCount = answerList.map(ans => {
            return {
                "answer": ans,
                "count": answerCountArr[answerList.indexOf(ans)],
            }    
        })

        dataByCount = dataByCount.filter(x => x!= null);
        setgraphDataBarCount(dataByCount);

        //date graph initial
        let data = seizureList.map(seizure => {
            if(seizure.data.feedback_question == null || seizure.data.feedback_question[question_text] == null) return;
            return {
            "date": moment(seizure.data.date.toDate()).format('DD/MM/YY hh:mm'),
            "answer": answerList.indexOf(seizure.data.feedback_question[question_text]) + 1,
        }});
        data = data.filter(x => x!= null);

            //sort the array
        data.sort(function(a,b){
        return new Date(b.date) - new Date(a.date);
      });

      setgraphDataByDate(data);

    }, [props.seizureListByParticipantDict,props.participantId,props.question_obj])

    const getDateGraph = () => {
        return (
            <React.Fragment>
            <ResponsiveContainer width="80%" height={400}>
            <LineChart width={1200} height={250} data={graphDataByDate}
                margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="date" />
                <YAxis dataKey="answer"/>
                <Tooltip />
                <Legend />
                <Line type="monotone" dataKey="answer" stroke="#8884d8" />
            </LineChart>
            </ResponsiveContainer>
            <Descriptions title={"the answers for the index in the graph: "} bordered column={1}>
                {answerList.map(answer => 
                                <Descriptions.Item label={answerList.indexOf(answer) + 1}>{answer}</Descriptions.Item>
                    )}
                {answerList.length === 0 &&  <Descriptions.Item label="there are no answer for the current question"></Descriptions.Item>}
            </Descriptions>
            </React.Fragment>
        )
    }
    const getCountGraph = () => {
        return (
            <React.Fragment>
            <ResponsiveContainer width="80%" height={400}>
            <BarChart width={1200} height={250} data={graphDataBarCount}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="answer" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Bar dataKey="count" fill="#8884d8" />
            </BarChart>
            </ResponsiveContainer>
            </React.Fragment>
        )
    }
    const onChangeRadio = e => {
        setradioValue(e.target.value);
      };
    const contentGraph = {
        graphDataByDate: getCountGraph,
        graphDataBarCount: getDateGraph,
    }

    return (
        <div>
            <Row>
            <Radio.Group onChange={onChangeRadio} value={radioValue}>
                <Radio.Button value="graphDataBarCount">Answer Counter</Radio.Button>
                <Radio.Button value="graphDataByDate">Answer By Date</Radio.Button>
            </Radio.Group>
            </Row>

            <Row type="flex" justify="center" align="middle" style={{height:"70vh"}}>
                <Col style={{width: "80%"}}>
            <Row >
            {contentGraph[radioValue]()}
            </Row>
            </Col>
            </Row>
        </div>
    )
}


export default SpecificParticipantCloseQuestionChart

