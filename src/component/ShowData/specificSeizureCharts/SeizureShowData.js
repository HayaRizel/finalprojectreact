import React, {useState,useEffect} from 'react'
import PropTypes from 'prop-types'
import { getSeizureDoc } from '../getDataFromDb';
import { Descriptions, Badge } from 'antd';



const average = arr => arr.reduce((a,b) => a + b, 0) / arr.length;

function SeizureShowData(props) {
    const seizureData = props.seizureListByParticipantDict[props.participantId].find(x=> x.id === props.seizureId).data;
    // useEffect(() => {
    //     if(props.seizureId == null || props.seizureId === "") return;
    //     getSeizureDoc(props.experimentKey,props.participantId ,props.seizureId)
    //         .then((ret) => {
    //             setseizureData(ret);
    //         })
    //         .catch((error) => {
    //             console.log("error getting parcipants list: ", error);
    //         });
    // }, [props.seizureId])

    const getQuestionDataShow = () => {
        if(seizureData != null && seizureData.app_info != null && seizureData.app_info.answer_question != null && Object.keys(seizureData.app_info.answer_question).length !== 0){
            return         <Descriptions title="Question Info" bordered column={1}>
            {Object.keys(seizureData.app_info.answer_question).map(question =>
                <Descriptions.Item label={question}>{seizureData.app_info.answer_question[question]}</Descriptions.Item>
            )
            }
        </Descriptions>
        }else{
            return <p>- no app question data -</p>;
        }
    }
    const getFeadbackQuestion = () => {
        if(seizureData != null && seizureData.feedback_question != null && Object.keys(seizureData.feedback_question).length !== 0){
            return         <Descriptions title="Feadback Question Info" bordered>
            {Object.keys(seizureData.feedback_question).map(question =>
                <Descriptions.Item label={question}>{seizureData.feedback_question[question]}</Descriptions.Item>
            )
            }
        </Descriptions>
        }else{
            return <p>- no feadback question data -</p>;
        }
    }

    const getStroopGameDataShow = () => {
        if(seizureData != null && seizureData.app_info != null && seizureData.app_info.stroopGame != null && (seizureData.app_info.stroopGame.conguent != null || seizureData.app_info.stroopGame.conguent != null)){
            return         <Descriptions title="Stroop Game Info" bordered column={3}>
                {seizureData.app_info.stroopGame.conguent != null && 
                <React.Fragment>
                                <Descriptions.Item label="conguent accurety">
                                    {seizureData.app_info.stroopGame.conguent.accurety + " from " + seizureData.app_info.stroopGame.conguent.totalComplete} 
                                    </Descriptions.Item>
                                {/* <Descriptions.Item label="conguent total complete">{seizureData.app_info.stroopGame.conguent.totalComplete}</Descriptions.Item> */}
                                <Descriptions.Item label="conguent time (MS)">{seizureData.app_info.stroopGame.conguent.timeInMilisecond.map(t => t + " ")}</Descriptions.Item>
                                <Descriptions.Item label="average conguent time (MS)">{average(seizureData.app_info.stroopGame.conguent.timeInMilisecond).toFixed(2)}</Descriptions.Item>

                                </React.Fragment>
                
                }
                                {seizureData.app_info.stroopGame.conguent != null && 
                <React.Fragment>

                <Descriptions.Item label="inconguent accurety">
                    {seizureData.app_info.stroopGame.inconguent.accurety + " from " + seizureData.app_info.stroopGame.inconguent.totalComplete}
                    </Descriptions.Item>
                {/* <Descriptions.Item label="inconguent total complete">{seizureData.app_info.stroopGame.inconguent.totalComplete}</Descriptions.Item> */}
                <Descriptions.Item label="inconguent time (MS)">{seizureData.app_info.stroopGame.inconguent.timeInMilisecond.map(t => t + " ")}</Descriptions.Item>
                <Descriptions.Item label="average inconguent time (MS)">{average(seizureData.app_info.stroopGame.inconguent.timeInMilisecond).toFixed(2)}</Descriptions.Item>

                </React.Fragment>

                                }
        </Descriptions>
        }else{
            return null;
        }

    }
    return (
        <div>
            {getQuestionDataShow()}
            <br/>
            {getFeadbackQuestion()}
            <br/>
            {getStroopGameDataShow()}
            

        </div>
    )
}

SeizureShowData.propTypes = {
    
}

export default SeizureShowData

