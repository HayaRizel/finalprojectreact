import React,{useState,useEffect} from 'react'
import { Switch, Col,Row, Modal } from 'antd';
import db from '../../../services/firestore.config';
import { v4 as uuid } from 'uuid';
import CardShowExperiments from './CardShowExperiments';
import TableExperiment from './TableExperiment';



function MainUserWindow(props) {
    const [finishLoading, setstartLoading] = useState(false);
    const userId = props.userId;
    const [expList, setexpList] = useState([])
    const [showExperiment, setshowExperiment] = useState("cards");
    const [isModalVisible, setisModalVisible] = useState(false);
    const [expKeyToDelete, setexpKeyToDelete] = useState(null)
  const getExpListFromDb = async (idUser) => {
        let researchArr = [];
        const db_experiments = db.collection('experiments_details');
        const data = await db_experiments.get();
        data.forEach((doc) => {
            const docData = doc.data();
            if (docData.metadata != null && docData.metadata.researcher_id != null) {
                if (idUser === docData.metadata.researcher_id) {
                    const researchElement = {
                        experimentKey: doc.id,
                        metadata: doc.data().metadata
                    }
                    researchArr = [...researchArr, researchElement]
                }
            }
        });
        setexpList(researchArr);
        setstartLoading(true);
      }

    useEffect(() => {
        //get the reaserch table
        getExpListFromDb(userId);
    }, [userId])

    const addNewExp = () => {
        window.location.href = "/addExperiment/" + uuid();
    }

    const deleteExperiment = (expKey) => {
        setexpKeyToDelete(expKey);
        setisModalVisible(true);
    }

    const handleOk = () => {
        const db_experiments = db.collection('experiments_details');
        db_experiments.doc(expKeyToDelete).delete().then(() => {
          getExpListFromDb(userId);
          setisModalVisible(false);
      }).catch((error) => {
          console.error("Error removing document: ", error);
      });  
      };
    
      const handleCancel = () => {
        setisModalVisible(false);
      };
        
    return (
        <React.Fragment>
            {finishLoading && 
        
        <div>
          <Row type="flex" justify="center" align="middle" >
            <Col>
          <h1>Your Experiments List</h1>
          </Col>
                    <Col offset={1}>
          <Switch 
          checkedChildren="cards show" 
          unCheckedChildren="table show" 
          onChange={(checked) => {(checked) ? setshowExperiment("table") : setshowExperiment("cards")}}
          />
          </Col>

          </Row>
          {showExperiment === "cards" && 
            <CardShowExperiments expList={expList} addNewResearch = {addNewExp} deleteExperiment = {deleteExperiment}/>}
            {showExperiment === "table" &&
             <TableExperiment expList={expList} addNewResearch = {addNewExp} deleteExperiment = {deleteExperiment}/> }

        <Modal title="Are you sure you ant to delete the experiment?" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
      </Modal>

        </div>
        
}</React.Fragment>
    )
}

export default MainUserWindow
