import React, { useState } from 'react'
import AlertAppStart from './AlertAppStart/AlertAppStart';
import ChooseMeasure from './chooseMeasure/ChooseMeasure';
import SelectActionShow from './selectActionShow/SelectActionShow';
import SelectFeadbackQuestion from './selectActionShow/SelectFeadbackQuestion';
import SelectFlow from './selectActionShow/SelectFlow';
import StepProgress from './StepsProgress';
import { useParams } from 'react-router-dom'
import EndStep from './EndStep';


function CreateAppDetails(props) {
    const userId = props.userId;
    const experimentKey = useParams().experimentKey;

    const [pickName, setPickName] = useState(false);
    const [current, setcurrent] = useState(0);
    const [completeStep, setcompleteStep] = useState([]);

    const onSubmit = (stepComleteIndex) => {
        setcompleteStep([...completeStep,stepComleteIndex]);
    }

    const dictStepComponent = {
        0: <SelectFlow expKey = {experimentKey} onSubmit={onSubmit.bind(this,2)}/>,
        1: <SelectFeadbackQuestion expKey = {experimentKey} onSubmit={onSubmit.bind(this,3)}/>,
        2: <EndStep expKey = {experimentKey} onSubmit={onSubmit.bind(this,4)}/>
    }   
    
    return (
        <React.Fragment>
            <StepProgress getCurrentStep={setcurrent} completeStep={completeStep} onChangeStep={onSubmit}/>
            <div style={{ margin: "50px", padding: "10px", border: "solid", borderWidth: "thin", height: "70vh", overflow: "auto" }}>
                {
                    dictStepComponent[current]
                    }
            </div>
        </React.Fragment>
    )
}

export default CreateAppDetails
