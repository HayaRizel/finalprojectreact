import React from 'react'
import { Button,Row,Col } from 'antd'

function EndStep(props) {
    return (
        <div>
            <Row align="middle" justify="center" style={{height: "70vh"}}>
                <Col>
                <p>You have finished setting up the experiment. You can go back and edit it at any time.</p>
            <Button 
            type="primary"
             size="large"
            onClick= {() => {window.location.replace('/');}}
            >
                Click here to complete the experiment
            </Button>
            </Col>
            </Row>
            </div>
    )
}

export default EndStep

