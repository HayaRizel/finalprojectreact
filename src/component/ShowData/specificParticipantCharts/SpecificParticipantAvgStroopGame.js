import React, {useState, useEffect} from 'react'
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer, Bar, BarChart } from 'recharts';
import moment from 'moment';
import { Row } from "antd";


const average = arr => arr.reduce((a,b) => a + b, 0) / arr.length;

function SpecificParticipantAvgStroopGame(props) {
    const [graphData, setgraphData] = useState([])

    useEffect(() => {
        if(props.seizureListByParticipantDict == null || props.participantId == null || props.seizureListByParticipantDict[props.participantId] == null) return;

        const seizureList = props.seizureListByParticipantDict[props.participantId];
        let data = seizureList.map(seizure => {
            if(seizure.data.app_info.stroopGame == null || seizure.data.app_info.stroopGame.conguent == null
                || seizure.data.app_info.stroopGame.inconguent == null) return;
            return {
            "date": moment(seizure.data.date.toDate()).format('DD/MM/YY hh:mm'),
            "conguent": average(seizure.data.app_info.stroopGame.conguent.timeInMilisecond).toFixed(2),
            "inconguent": average(seizure.data.app_info.stroopGame.inconguent.timeInMilisecond).toFixed(2),
        }});
            //sort the array
        data.sort(function(a,b){
        return new Date(b.date) - new Date(a.date);
      });

        setgraphData(data);
    }, [props.seizureListByParticipantDict,props.participantId])

    return (
        <div>
            <Row type="flex" justify="center" align="middle" style={{height:"70vh"}}>
            <ResponsiveContainer width="80%" height={400}>
            <LineChart width={1200} height={250} data={graphData}
                margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="date" />
                <YAxis />
                <Tooltip />
                <Legend />
                <Line type="monotone" dataKey="conguent" stroke="#8884d8" />
                <Line type="monotone" dataKey="inconguent" stroke="#82ca9d" />
            </LineChart>
            </ResponsiveContainer>
            </Row>
        </div>
    )
}

SpecificParticipantAvgStroopGame.propTypes = {

}

export default SpecificParticipantAvgStroopGame

