import React,{useState,useEffect} from 'react'
import {  Button, Table } from 'antd';
import { CloseOutlined,InfoOutlined,EditOutlined } from '@ant-design/icons';

function TableExperiment(props) {
    const [dataSource, setdataSource] = useState([])
    // let dataSource = [
    //     {
    //       key: '1',
    //       name: 'Mike',
    //       edit: 32,
    //       delete: '10 Downing Street',
    //     },
    //   ];
      
      const columns = [
        {
          title: 'Research Name',
          dataIndex: 'name',
          key: 'name',
        },
        {
            title: 'description',
            dataIndex: 'description',
            key: 'description',
          },
  
        {
          title: 'edit research',
          dataIndex: 'edit',
          key: 'edit',
          render: (idResearch) => (
            <Button 
            icon = {<EditOutlined />}
            onClick={() => {window.location.href = '/editExperiment/' + idResearch;} 
                        }>
            </Button>
        ),

        },
        {
          title: 'show research data',
          dataIndex: 'data',
          key: 'data',
          render: (idResearch) => (
              <Button 
              icon = {<InfoOutlined />}
              onClick={() => {window.location.href = '/researchData/'+idResearch;} 
                          }>
              </Button>
          ),
        },
        {
            title: 'delete research',
            dataIndex: 'delete',
            key: 'delete',
            render: (idResearch) => (
                <Button 
                icon={<CloseOutlined />}
                onClick={props.deleteExperiment.bind(this, idResearch) }
                            >
                    
                </Button>
            ),
  
        },
      ];

    useEffect(() => {
        const initDataSource = props.expList.map((researchElement) => {
            return {
                key: researchElement.experimentKey,
                name: researchElement.metadata.experiment_name,
                description: researchElement.metadata.description,
                edit: researchElement.experimentKey,
                data: researchElement.experimentKey,
                delete: researchElement.experimentKey,
                // description: researchElement.metadata.description,
            }
        })    
        setdataSource(initDataSource);
        }, [props.expList])


    return (
        <div>
                        <Button onClick={props.addNewResearch}>add new research</Button>
                        <Table dataSource={dataSource} columns={columns} />

        </div>
    )
}

TableExperiment.propTypes = {

}

export default TableExperiment

