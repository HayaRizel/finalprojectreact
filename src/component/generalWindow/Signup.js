import React, {useState} from 'react'
import { Form, Input, Button,Col,Row,Divider  } from 'antd';
import { useFormik } from 'formik';
import "firebase/auth";
import "firebase/firestore"
import firebase from 'firebase/app'
import { GoogleOutlined } from '@ant-design/icons';

function Signup(props) {
    const [errorMessageShow, seterrorMessageShow] = useState("")
    const moveToMainWindow = (idUser, email) => {
        props.onFinish(idUser);
    }
    const googleSignUp = () => {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth()
        .signInWithPopup(provider)
        .then((result) => {
          /** @type {firebase.auth.OAuthCredential} */
          var credential = result.credential;
      
          // This gives you a Google Access Token. You can use it to access the Google API.
          var token = credential.accessToken;
          // The signed-in user info.
          var user = result.user;
          var userId = user.uid;
          var email = user.email;
          var displayName = user.displayName;
          moveToMainWindow(userId, email);
          // ...
        }).catch((error) => {
          // Handle Errors here.
          var errorCode = error.code;
          var errorMessage = error.message;
          // The email of the user's account used.
          var email = error.email;
          // The firebase.auth.AuthCredential type that was used.
          var credential = error.credential;
          // ...
        });
      
    }


    const formik = useFormik({
        initialValues: {
            email: "",
            password: "",
        },
        onSubmit:  (values) => {
            firebase.auth().createUserWithEmailAndPassword(values.email, values.password)
            .then((userCredential) => {
                // Signed in 
                var user = userCredential.user;
                var userId = user.uid;
                var email = user.email;
                seterrorMessageShow("");
                moveToMainWindow(userId, email);
                // ...
            })
            .catch((error) => {
                var errorCode = error.code;
                var errorMessage = error.message;
                seterrorMessageShow(errorMessage);
                // ..
            });


        }
    });


    return (
        <div>
            <Row type="flex" justify="center" align="middle" style={{minHeight: '100vh'}}>
<Col>

<Row>
            <Form 
            name="basic"
            onFinish={formik.handleSubmit}>
                                    <Form.Item
                            label="private name"
                            name="privateName"                    
                            rules={[{ required: true, message: 'Please input your private name!' }]}
                            >
                            <Input
                                id = "privateName"
                                onChange = {formik.handleChange}
                                value = {formik.values.privateName}   
                             />
                    </Form.Item>
                    <Form.Item
                            label="last name"
                            name="lastName"                    
                            rules={[{ required: true, message: 'Please input your last name!' }]}
                            >
                            <Input
                                id = "lastName"
                                onChange = {formik.handleChange}
                                value = {formik.values.lastName}   
                             />
                    </Form.Item>

                    <Form.Item
                            label="email"
                            name="email"                    
                            rules={[{ required: true, message: 'Please input your email!',type: 'email' }]}
                            >
                            <Input
                                id = "email"
                                onChange = {formik.handleChange}
                                value = {formik.values.email}   
                             />
                    </Form.Item>

                <Form.Item
        label="Password"
        name="password"
        rules={[{ required: true, message: 'Please input your password!' }]}
      >
        <Input.Password 
            id = "password"
            onChange = {formik.handleChange}
            value = {formik.values.password}                                                
        />
      </Form.Item>
      <Form.Item >
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
      <p>{errorMessageShow}</p>

            </Form>
            </Row>
            <Divider plain="false">OR</Divider>

            <Row type="flex" justify="center" align="bottom">
            <Button type="primary" htmlType="submit" onClick={googleSignUp} icon={<GoogleOutlined />}>
          Log in with Google
        </Button>
        </Row>
        <Row type="flex" justify="center" align="bottom">
                <p>Have an account?  <a href = "/">Log in</a></p>
            </Row>

            </Col>
            </Row>
            </div>
    )
}

Signup.propTypes = {

}

export default Signup

