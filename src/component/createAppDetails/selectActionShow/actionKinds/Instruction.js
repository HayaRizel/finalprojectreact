import React, {useEffect} from 'react'
import PropTypes from 'prop-types'
import { Form, Input, Button, Checkbox, Select, InputNumber, TimePicker, Radio, DatePicker, Tooltip, Space } from 'antd';
import { useFormik } from 'formik';
import addActionToDb from '../addActionToDb';

const { TextArea } = Input;




export default function Instruction(props) {
    const formik = useFormik({
        initialValues: {
        },
        onSubmit:  (values) => {
            const instruction_obj = {
                "action_name": "instruction",
                "action_id": props.id,
                "action_value": {
                    "instruction_text": values.instruction
                }
            }
            addActionToDb(instruction_obj,props.flow_key,props.expKey)
        }
    });
    
    useEffect(() => {
        if(props.editstatus === false){
            formik.handleSubmit();
        }
    }, [props.editstatus])
      
    useEffect(() => {
        if(props.initialValue == null) return;
            formik.setFieldValue("instruction",props.initialValue.instruction_text);
    }, [])

      return (
        <Form onFinish={formik.handleSubmit}>
            <Form.Item
                label="write instruction (the user need to do that after the seizure"
            >
                <Input.TextArea
                    id="instruction"
                    onChange = {formik.handleChange}
                    value = {formik.values.instruction}                
                    disabled = {!props.editstatus}
                    rows = {4} />
            </Form.Item>
        </Form>
    )
}



