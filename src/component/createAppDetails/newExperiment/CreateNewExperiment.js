import React, { useState, useEffect } from 'react';
import { Input, Row, Form, Button,Col } from 'antd'
import db from '../../../services/firestore.config';
import { useParams } from 'react-router-dom'


const coverImageArr = [
"https://firebasestorage.googleapis.com/v0/b/finalproject-ccaee.appspot.com/o/experimentsCoverImage%2FcoverExperimentImages_1.jpg?alt=media&token=9270cea4-1796-4275-82c5-e83a8aa844dc",
"https://firebasestorage.googleapis.com/v0/b/finalproject-ccaee.appspot.com/o/experimentsCoverImage%2FcoverExperimentImages_10.jpg?alt=media&token=c57486fc-cf70-4696-aadd-6ede7a69060f",
"https://firebasestorage.googleapis.com/v0/b/finalproject-ccaee.appspot.com/o/experimentsCoverImage%2FcoverExperimentImages_11.jpg?alt=media&token=907ccc41-b913-464c-aaa9-588fec8034f3",
"https://firebasestorage.googleapis.com/v0/b/finalproject-ccaee.appspot.com/o/experimentsCoverImage%2FcoverExperimentImages_11.jpg?alt=media&token=907ccc41-b913-464c-aaa9-588fec8034f3",
"https://firebasestorage.googleapis.com/v0/b/finalproject-ccaee.appspot.com/o/experimentsCoverImage%2FcoverExperimentImages_13.jpg?alt=media&token=b51d4cde-658c-4b3d-b110-fb572aa7470b",
"https://firebasestorage.googleapis.com/v0/b/finalproject-ccaee.appspot.com/o/experimentsCoverImage%2FcoverExperimentImages_14.jpg?alt=media&token=eb5b2837-36ef-40f4-af02-76c4dc32dda6",
"https://firebasestorage.googleapis.com/v0/b/finalproject-ccaee.appspot.com/o/experimentsCoverImage%2FcoverExperimentImages_15.jpg?alt=media&token=8676310e-5e5b-4e50-9bb2-edeb713e7527",
"https://firebasestorage.googleapis.com/v0/b/finalproject-ccaee.appspot.com/o/experimentsCoverImage%2FcoverExperimentImages_2.jpg?alt=media&token=e15dfb33-2deb-4234-b77a-7b4944860d1d",
"https://firebasestorage.googleapis.com/v0/b/finalproject-ccaee.appspot.com/o/experimentsCoverImage%2FcoverExperimentImages_3.jpg?alt=media&token=c383d47c-8b3f-4b56-809d-edb3d28ede64",
"https://firebasestorage.googleapis.com/v0/b/finalproject-ccaee.appspot.com/o/experimentsCoverImage%2FcoverExperimentImages_4.jpg?alt=media&token=451b1138-8d07-487b-9d90-9ae3310c9eb3",
"https://firebasestorage.googleapis.com/v0/b/finalproject-ccaee.appspot.com/o/experimentsCoverImage%2FcoverExperimentImages_5.jpg?alt=media&token=a955f1bb-5557-4040-897f-06c60aada3c1",
"https://firebasestorage.googleapis.com/v0/b/finalproject-ccaee.appspot.com/o/experimentsCoverImage%2FcoverExperimentImages_6.jpg?alt=media&token=2d0aa6e6-8de4-4bee-8813-e6b0e559e2d2",
"https://firebasestorage.googleapis.com/v0/b/finalproject-ccaee.appspot.com/o/experimentsCoverImage%2FcoverExperimentImages_7.jpg?alt=media&token=640bb2e1-9097-4790-bec5-4a844e0c6504",
"https://firebasestorage.googleapis.com/v0/b/finalproject-ccaee.appspot.com/o/experimentsCoverImage%2FcoverExperimentImages_8.jpg?alt=media&token=a337f1cc-aba2-4545-b4af-1ca1a0c1bb09",
"https://firebasestorage.googleapis.com/v0/b/finalproject-ccaee.appspot.com/o/experimentsCoverImage%2FcoverExperimentImages_9.jpg?alt=media&token=6310b178-b83e-4dbe-8b3c-f785b2eb7160"
]

function CreateNewExperiment(props) {
    const userId = props.userId;
    const experimentKey = useParams().experimentKey;
    const db_experiments = db.collection('experiments_details');
    const db_data_collected = db.collection('Data_collected');

    const [validName, setValidName] = useState();
    const [experimentName, setExperimentName] = useState("")
    let currentName = "";
    const [description, setdescription] = useState("")

    const isExperimentExist = async (id) => {
        const data = await db_experiments.get();
        let ret = false;
        data.forEach((doc) => {
            const docData = doc.data();
            if (docData.metadata != null && docData.metadata.experiment_name != null) {
                if (id === docData.metadata.experiment_name) ret = true;
            }
        })
        return ret;
    }


    function getRandomInt(max) {
        return Math.floor(Math.random() * max);
      }
    
    const onSubmit = (e) => {
        if(validName != "success") return;
        if(description.length === 0) return;
        isExperimentExist(experimentName)
            .then(ret => {
                if (ret === true) return;
                else {
                    updateDb();
                    
                }
            })
            .catch(err => console.error(err))
    }
    const updateDb = () => {
        db_experiments.doc(experimentKey)
            .set({
                metadata: {
                    creation_date: new Date().toJSON().slice(0, 10).replace(/-/g, '/'),
                    experiment_name: experimentName,
                    researcher_id: userId,
                    cover_image: coverImageArr[getRandomInt(coverImageArr.length)],
                    description: description,
                },
            }, { merge: true }).then(ret => {
                db_data_collected.doc(experimentKey)
                .set({participants_id: []})
                .then(ret =>{
                    setValidName("success");
                    window.location.replace('/editExperiment/' + experimentKey);    
                })
                .catch(err => console.error(err));
            })
            .catch(err => console.error(err));
    }

    const onChangeExperimentName = (name) => {
        setExperimentName(name);
        currentName = name;
        if (name === "") {
            setValidName("error");
            return;
        }
        setValidName("validating");
        isExperimentExist(name)
            .then(ret => {
                //this is not the most update check
                if (name !== currentName) {
                    return;
                }
                if (ret === true) {
                    setValidName("error");
                }
                else {
                    setValidName("success");
                }
            })
            .catch(err => {console.error(err);setValidName("validating");})
    }

    const getHelp = () => {
        if (experimentName === "") {
            return "need to choose a name"
        }
        if (validName === "error") {
            return "the name exist - choose another"
        }
        return ""
    }

    return (
        <React.Fragment>
            <br/>
            <Row>
                <Col offset= {1}>
            <Form onFinish={onSubmit}>
                    <Form.Item
                        hasFeedback
                        validateStatus={validName}
                        help={getHelp()}
                        label="experiment name">
                        <Input
                            id="experiment_name"
                            name="experiment_name"
                            type="text"
                            // validate={isExperimentExist}
                            onChange={(e) => {
                                onChangeExperimentName(e.target.value);
                            }}
                            value={experimentName}
                        />
                    </Form.Item>
                    <Form.Item
                        label="description"
                        help="some words about the experiment">
                        <Input.TextArea rows={4} 
                            id="description"
                            name="description"
                            type="text"
                            onChange={(e) => {
                                setdescription(e.target.value);
                            }}
                            value={description}
                        />
                    </Form.Item>
<Row justify="end">
                    <Button type="primary" htmlType="submit">
                        Submit
        </Button>
        </Row>
            </Form>
            </Col>
            </Row>
        </React.Fragment>
    )
}


export default CreateNewExperiment

