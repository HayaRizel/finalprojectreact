import React,{useEffect,useState} from 'react'
import { Descriptions } from 'antd';
import {  XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer,Bar,BarChart } from 'recharts';
import { Row,Col } from "antd";


function getSeizureByMonth(seizureListByParticipantDict){
    let monthCountArr = [0,0,0,0,0,0,0,0,0,0,0,0]
    const monthNameArr = ["January", "February", "March", "April","May", "June", "July", "August", "September", "October", "November", "December"];

    Object.keys(seizureListByParticipantDict).forEach(key => {
        seizureListByParticipantDict[key].forEach(seizure => {
            monthCountArr[seizure.data.date.toDate().getMonth()]++;
        })
    })
        
    const data = [...Array(12).keys()].map(monthIndex => {
        return {
            monthName: monthNameArr[monthIndex],
            "seizure count": monthCountArr[monthIndex] 
        }
    })
    return data;
}

function getAllSeizureCount(dataSeizureByMonth){
    let counter = 0;
    dataSeizureByMonth.forEach(element => {
        counter += element["seizure count"];
    })
    return counter;
}


function SeizureByMonthChart(props) {


    const [dataSeizureByMonth, setdataSeizureByMonth] = useState([])
    const allSeizureCount = getAllSeizureCount(dataSeizureByMonth);
    
    
    useEffect(() => {
        if(props.seizureListByParticipantDict == null) return;
        const ret = getSeizureByMonth(props.seizureListByParticipantDict);
        setdataSeizureByMonth(ret);
    }, [props.seizureListByParticipantDict])



    return (
        <Row type="flex" justify="center" align="middle" style={{height:"70vh"}}>
            <Col style={{width: "80%"}}>
            <Row>
            <ResponsiveContainer width="80%" height={400}>
            <BarChart width={950} height={250} data={dataSeizureByMonth}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="monthName" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Bar dataKey="seizure count" fill="#8884d8" />
            </BarChart>
            </ResponsiveContainer>
            </Row>
            <Row justify="center" align="middle">
        <Descriptions  bordered>
                <Descriptions.Item label="how much seizure">{getAllSeizureCount(dataSeizureByMonth)}</Descriptions.Item>
        </Descriptions>
        </Row>
        </Col>
        </Row>
    )
}


export default SeizureByMonthChart

