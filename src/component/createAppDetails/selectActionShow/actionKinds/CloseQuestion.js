import React, { useState, useEffect} from 'react'
import { Form, Input, Button, Tooltip, Row, Checkbox, Space, Upload } from 'antd';
import { MinusOutlined, PlusOutlined } from '@ant-design/icons';
import { v4 as uuid } from 'uuid';
import { useFormik } from 'formik';
import addActionToDb from '../addActionToDb';

//note: can show the answer randomally
//if not randomaly change with drag and drop
function CloseQuestion(props) {
    const [answerArr, setanswerArr] = useState([uuid(), uuid()]);
    const [showFeadback, setshowFeadback] = useState(false);

    const formik = useFormik({
        initialValues: {
            show_feadback_option: false,
            question_text: "",
        },
        onSubmit:  (values) => {
            let correct_answers = [];
            const answerArr_obj = answerArr.map(answerId=>{
                if(values["show_feadback_option" + answerId] == null){
                    values["show_feadback_option" + answerId] = false;
                }
                if(values["show_feadback_option" + answerId] == true){
                    correct_answers = [...correct_answers, answerId]
                }
                return {
                    "answer_id": answerId,
                    "answer": values["answer"+answerId],
                }
            })
            const instruction_obj = {
                "action_name": "question_close",
                "action_id": props.id,
                "action_value": {
                    "question_text": values.question_text,
                    "answer" : {
                        // "show_feadback_option": values.show_feadback_option,
                        "answers": answerArr_obj,
                        // "correct_answers" : correct_answers
                    }
                }

            }
            addActionToDb(instruction_obj,props.flow_key,props.expKey)
        }
    });

    useEffect(() => {
        const value = props.initialValue;
        if(value == null) return;

        formik.setFieldValue("question_text",value.question_text);
        // formik.setFieldValue("show_feadback_option",value.answer.show_feadback_option);
        setshowFeadback(value.answer.show_feadback_option);
        let answerArr = [];
        value.answer.answers.forEach(element => {
            const answerId = element.answer_id;
            answerArr.push(answerId);
            formik.setFieldValue("answer"+answerId, element.answer);
        });
        // value.answer.correct_answers.forEach(answerId => {
        //     formik.setFieldValue("show_feadback_option" + answerId, true);
        // });
        setanswerArr(answerArr);

    }, [])

    useEffect(() => {
        if(props.editstatus === false){
            formik.handleSubmit();
        }
    }, [props.editstatus])

    return (
        <Form>
            <Row>
                <Form.Item
                    label="write your question">
                    <Input.TextArea 
                    id = "question_text"
                    onChange = {formik.handleChange}
                    value = {formik.values.question_text}                                                            
                    rows={4} 
                    disabled={!props.editstatus} />
                </Form.Item>
            </Row>
            <Row>
            </Row>
            <Form.Item>
                {/* <Row>
                    <Checkbox
                        checked={formik.values.show_feadback_option}
                        onChange = {(e) => {formik.setFieldValue("show_feadback_option",e.target.checked)}}                    
                        disabled={!props.editstatus}
                        onChange={(e) => {
                            formik.setFieldValue("show_feadback_option",e.target.checked);
                             setshowFeadback(!showFeadback); }}>
                        show feadback to the user
                    </Checkbox>
                </Row> */}
            </Form.Item>
            <Row>
                <p>write your answers</p>
            </Row>
            {answerArr.map(answerId => (
                <Row key={answerId}>
                    <Space align="start">
                        <Tooltip title="delete answer" placement="left" mouseEnterDelay={1}>
                            <Button shape="circle" icon={<MinusOutlined />}
                                disabled={!props.editstatus}
                                onClick={() => { setanswerArr(answerArr.filter(ans => ans !== answerId)) }}>
                            </Button>
                        </Tooltip>
                        <Form.Item>
                            <Input 
                            value={formik.values["answer"+answerId]}
                            onChange = {(e) => {formik.setFieldValue("answer"+answerId,e.target.value)}}                            
                            disabled={!props.editstatus} />
                        </Form.Item>
                        {/* {(showFeadback === true) &&
                            <Tooltip title="select if it is a good answer" placement="top"
                                mouseEnterDelay={1}>
                                <Checkbox 
                                checked={formik.values["show_feadback_option" + answerId]}
                                onChange = {(e) => {formik.setFieldValue("show_feadback_option" + answerId,e.target.checked)}}                                                    
                                disabled={!props.editstatus} />
                            </Tooltip>} */}
                    </Space>
                </Row>
            ))}
            <Row>
                <Tooltip title="add new answer" placement="left" mouseEnterDelay={1}>
                    <Button shape="circle" icon={<PlusOutlined />}
                        disabled={!props.editstatus}
                        hidden = {(answerArr.length >= 4)}
                        onClick={() => { setanswerArr([...answerArr, uuid()]) }}>
                    </Button>
                </Tooltip>
            </Row>
        </Form>
    )
}


export default CloseQuestion