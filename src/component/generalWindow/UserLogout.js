import "firebase/auth";
import "firebase/firestore"
import firebase from 'firebase/app'

export default function UserLogout() {
    firebase.auth().signOut().then(() => {
        window.location.replace('/');
        // Sign-out successful.
      }).catch((error) => {
        // An error happened.
      });
      
}
