import React, { useState,useEffect } from 'react'
import PropTypes from 'prop-types'
import { Row, TimePicker, Checkbox, Form ,Button} from 'antd';
import moment from 'moment';
import { useFormik } from 'formik';
import {db_experiments} from '../../../services/firestore.config';


function ChooseMeasure(props) {
    const [pulse, setpulse] = useState(false)
    const [location, setlocation] = useState(false)
    const [time, settime] = useState(false)
    const [day, setday] = useState(false)
    const [date, setdate] = useState(false)
    const [vacation, setvacation] = useState(false)
    const [frenzy, setfrenzy] = useState(false)
    const dictSetHook = {
        "pulse": setpulse,
        "location": setlocation,
        "time": settime,
        "day": setday,
        "date": setdate,
        "vacation": setvacation,
        "frenzy": setfrenzy,
    }
    let dictVar = {
        "pulse": pulse,
        "location": location,
        "time": time,
        "day": day,
        "date": date,
        "vacation": vacation,
        "frenzy": frenzy,
    }
    const dictTimeSelect = {
        "pulse": true,
        "location": true,
        "time": false,
        "day": false,
        "date": false,
        "vacation": false,
        "frenzy": true,
    }
    const formik = useFormik({
        initialValues: {
            // time_pulse: moment('00:00:00', 'HH:mm:ss'),
            // time_location: moment('00:00:00', 'HH:mm:ss'),
            // time_time: moment('00:00:00', 'HH:mm:ss'),
            // time_day: moment('00:00:00', 'HH:mm:ss'),
            // time_date: moment('00:00:00', 'HH:mm:ss'),
            // time_vacation: moment('00:00:00', 'HH:mm:ss'),
            // time_frenzy: moment('00:00:00', 'HH:mm:ss'),
            // freq_pulse: moment('00:00:00', 'HH:mm:ss'),
            // freq_location: moment('00:00:00', 'HH:mm:ss'),
            // freq_time: moment('00:00:00', 'HH:mm:ss'),
            // freq_day: moment('00:00:00', 'HH:mm:ss'),
            // freq_date: moment('00:00:00', 'HH:mm:ss'),
            // freq_vacation: moment('00:00:00', 'HH:mm:ss'),
            // freq_frenzy: moment('00:00:00', 'HH:mm:ss'),
        },
        onSubmit:  (values) => {
            const measuresArr = Object.keys(dictVar).map(measureName => {
                if(dictVar[measureName] === false) return;
                if(dictVar[measureName] === true && dictTimeSelect[measureName] === false){
                    return {
                        measure_name: measureName,
                    }
                }
                if(values["freq_" + measureName] != null && "time_" + measureName != null){
                return {
                    measure_name: measureName,
                    frequency: values["freq_" + measureName].format('HH:mm:ss'),
                    how_much_time: values["time_" + measureName].format('HH:mm:ss'),
                }
            }
            })
            //delete all the empty measure from the array
            const measuresArr_final = measuresArr.filter(node => node != null)
            db_experiments.doc(props.expKey)
                .set({
                    measures: measuresArr_final
                }, { merge: true })
                .then(ret => {
                    console.log(ret);
                    props.onSubmit();
                })
                .catch(err => {
                    console.error(err)
                });
        }
    });

    useEffect(() => {
        db_experiments.doc(props.expKey)
                .get()
                .then(ret => {
                    const measures_arr = ret.data().measures;
                    measures_arr.forEach(measure => {
                        dictSetHook[measure.measure_name](true);
                        if(dictTimeSelect[measure.measure_name] === true){
                            formik.setFieldValue("time_"+measure.measure_name, moment(measure.how_much_time, 'HH:mm:ss'))
                            formik.setFieldValue("freq_"+measure.measure_name, moment(measure.frequency, 'HH:mm:ss'))    
                        }
                    });
                    console.log(ret);
                })
                .catch(err => {
                    console.error(err)
                });
    }, [])

    return (
        <React.Fragment>
            <Row>
                <p>choose the measure you want and decide how much time after the seizure to measure it</p>
            </Row>
            <Form onFinish={formik.handleSubmit}>
                {Object.keys(dictVar).map(measure => (
                    <Row key={measure}>
                        <Form.Item >
                            <Checkbox
                                checked={dictVar[measure]}
                                onChange={() => { dictSetHook[measure](!dictVar[measure]) }}>
                                {measure}
                            </Checkbox>
                        </Form.Item>
                        {(dictVar[measure] === true && dictTimeSelect[measure] === true) &&
                            <React.Fragment>
                                <Form.Item
                                    label="how much time to measure"
                                >
                                    <TimePicker
                                        id={"time_" + measure}
                                        name={"time_" + measure}
                                        onChange={(date, dateString) => {formik.setFieldValue("time_"+measure,moment(dateString, 'HH:mm:ss'))}}
                                        value={formik.values["time_" + measure]}
                                        defaultOpenValue={moment('00:00:00', 'HH:mm:ss')}
                                    />
                                </Form.Item>
                                <Form.Item
                                    label="frequency measurement"
                                >
                                    <TimePicker
                                        id={"freq_" + measure}
                                        name={"freq_" + measure}
                                        onChange={(date, dateString) => {formik.setFieldValue("freq_"+measure,moment(dateString, 'HH:mm:ss'))}}
                                        value={formik.values["freq_" + measure]}
                                        defaultOpenValue={moment('00:00:00', 'HH:mm:ss')}
                                    />
                                    </Form.Item>
                            </React.Fragment>
                        }
                    </Row>

                ))}
        <Button type="primary" htmlType="submit">save</Button>
            </Form>
        </React.Fragment>
    )
}

ChooseMeasure.propTypes = {

}

export default ChooseMeasure

