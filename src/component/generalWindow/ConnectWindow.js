import React, {useState} from 'react'
import { Form, Input, Button, Checkbox, Select, InputNumber, TimePicker, Radio, DatePicker, Tooltip, Space,Row,Col,Carousel,Divider } from 'antd';
import { useFormik } from 'formik';
import "firebase/auth";
import "firebase/firestore"
import firebase from 'firebase/app'
import { Layout } from 'antd';
import { GoogleOutlined } from '@ant-design/icons';
import {  RiseOutlined } from '@ant-design/icons';

const { Header, Content, Footer, Sider } = Layout;

const carouselContentStyle = {
    height: '30vh',
    color: '#fff',
    lineHeight: '160px',
    textAlign: 'center',
    background: '#364d79',
    "letter-spacing": "2px",

    // 'font-family': "Noto Sans TC",
  };


function ConnectWindow(props) {
    const [errorMessageShow, seterrorMessageShow] = useState("")

    const moveToMainWindow = (idUser, email) => {
        props.onFinish(idUser);
    }
    const googleConnect = () => {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth()
        .signInWithPopup(provider)
        .then((result) => {
          /** @type {firebase.auth.OAuthCredential} */
          var credential = result.credential;
      
          // This gives you a Google Access Token. You can use it to access the Google API.
          var token = credential.accessToken;
          // The signed-in user info.
          var user = result.user;
          var userId = user.uid;
          var email = user.email;
          var displayName = user.displayName;
          moveToMainWindow(userId, email);
          // ...
        }).catch((error) => {
          // Handle Errors here.
          var errorCode = error.code;
          var errorMessage = error.message;
          // The email of the user's account used.
          var email = error.email;
          // The firebase.auth.AuthCredential type that was used.
          var credential = error.credential;
          // ...
        });
      
    }
    const formik = useFormik({
        initialValues: {
            email: "",
            password: "",
        },
        onSubmit:  (values) => {
            firebase.auth().signInWithEmailAndPassword(values.email, values.password)
            .then((userCredential) => {
                // Signed in 
                var user = userCredential.user;
                var userId = user.uid;
                moveToMainWindow(userId, values.email);
                // ...
            })
            .catch((error) => {
                var errorCode = error.code;
                var errorMessage = error.message;
                seterrorMessageShow(errorMessage);
                // ..
            });


        }
    });

      
    return (
        <div>
                <Carousel autoplay >
    <div>
      <h2 style={carouselContentStyle}> <RiseOutlined />B r e a k T h r o u g h  &nbsp;&nbsp;  Y o u r &nbsp;&nbsp;   M e n t a l   &nbsp;&nbsp; H e a l t h</h2>
    </div>
    <div>
      <h2 style={carouselContentStyle}>Sign up and make the Experiment you always want to do</h2>
    </div>
    <div>
      <h2 style={carouselContentStyle}>An website for defining an application that people will download and participate in an experiment</h2>
    </div>
  </Carousel>


<Row type="flex" justify="center" align="middle" style={{minHeight: '60vh'}}>
    <Col>
    <Row>
            <Form 
            name="basic"
            // labelCol={{ span: 8 }}
            // wrapperCol={{ span: 16 }}
            onFinish={formik.handleSubmit}>
                    <Form.Item
                            label="email"
                            name="email"                    
                        rules={[{ required: true, message: 'Please input your email!',type: 'email' }]}
                    >
                            <Input
                                id = "email"
                                onChange = {formik.handleChange}
                                value = {formik.values.email}                                        
                             />
                    </Form.Item>

                <Form.Item
        label="Password"
        name="password"
        rules={[{ required: true, message: 'Please input your password!' }]}
      >
        <Input.Password 
            id = "password"
            onChange = {formik.handleChange}
            value = {formik.values.password}                                                
        />
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
      <p>{errorMessageShow}</p>

            </Form>
            </Row>
            <Divider plain="false">OR</Divider>
            <Row type="flex" justify="center" align="bottom">
            <Button type="primary" htmlType="submit" onClick={googleConnect} icon={<GoogleOutlined />}>
          connect with google
        </Button>
        </Row>
        <Row type="flex" justify="center" align="bottom">
                <p>Don't have an account? <a href = "/accounts/signup">Sign up</a></p>
            </Row>
            </Col>
            </Row>

        </div>
    )
}

ConnectWindow.propTypes = {

}

export default ConnectWindow

