import React,{useState} from 'react'
import { Menu,PageHeader } from 'antd';
import {  RiseOutlined,UnorderedListOutlined } from '@ant-design/icons';
import { useParams } from 'react-router-dom'
import UserLogout from './UserLogout';

const { SubMenu } = Menu;



function UserNavBar(props) {
    const experimentKey = useParams().experimentKey;

    const handleClick = e => {
        switch (e.key) {
            case 'logOut':
                UserLogout();
              break;
            case 'main':
                window.location.replace('/main')
              break;
            // default:
            //   console.log('log');
        }
      };
    
    return (
      <React.Fragment>
    <PageHeader
    subTitle="BreakThrough Your Mental Health"
    avatar={{ icon: <RiseOutlined /> }}
    extra={[
      <Menu onClick={handleClick}  mode="horizontal">

      <Menu.Item key="main" icon={<UnorderedListOutlined />}>
        Experiment List
      </Menu.Item>
      <Menu.Item key="logOut" >
          Log Out
      </Menu.Item>
    </Menu>

    ]}
  >

      </PageHeader>
      </React.Fragment>
    )
}

export default UserNavBar

