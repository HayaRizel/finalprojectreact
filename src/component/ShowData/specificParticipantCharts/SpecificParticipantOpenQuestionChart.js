import React, {useState,useEffect} from 'react'
import PropTypes from 'prop-types'
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer, Bar, BarChart } from 'recharts';
import moment from 'moment';
import { Row,Descriptions } from "antd";

function SpecificParticipantOpenQuestionChart(props) {
    let question_obj = props.question_obj;
    // question_obj =  {
    //     action_id: "0fd100cb-378a-440f-b295-2644eb5f7c04",
    //     action_name: "question_open",
    //     action_value: {
    //         question_text: "How distressed are you feeling now?",
    //         answer: {
    //             save_answer_option: 10,
    //             min: 0,
    //             step: 1
    //         }
    //     }
    // }
    const question_text = question_obj.action_value.question_text;
    const [questionAnswerArr, setquestionAnswerArr] = useState([])
    useEffect(() => {
        if(props.seizureListByParticipantDict == null || props.participantId == null || props.seizureListByParticipantDict[props.participantId] == null) return;

        const seizureList = props.seizureListByParticipantDict[props.participantId];
        let data = seizureList.map(seizure => {
            if(seizure.data.feedback_question == null || seizure.data.feedback_question[question_text] == null) return;
            return {
            "date": moment(seizure.data.date.toDate()).format('DD/MM/YY hh:mm'),
            "answer": seizure.data.feedback_question[question_text],
        }});
        data = data.filter(x => x!= null);
        //sort the array
        data.sort(function(a,b){
        return !(new Date(b.date) - new Date(a.date));
      });
      data = data.map(ansObj => {return ansObj.answer})

      setquestionAnswerArr(data);
    }, [props.seizureListByParticipantDict,props.participantId,props.question_obj])



    return (
        <div>
            <Row type="flex" justify="center" align="middle" style={{height:"70vh"}}>
            <Descriptions title={"the answers for the open question: " + question_text} bordered column={1}>
                {questionAnswerArr.map(answer => 
                                <Descriptions.Item label={answer}></Descriptions.Item>
                    )}
                {questionAnswerArr.length === 0 &&  <Descriptions.Item label="there are no answer for the current participant"></Descriptions.Item>}

            </Descriptions>

            </Row>

        </div>
    )
}

export default SpecificParticipantOpenQuestionChart

