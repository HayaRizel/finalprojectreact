import React, { useState,useEffect } from 'react'
import PropTypes from 'prop-types'
import { Form, Input, Button, Checkbox, Select, InputNumber, TimePicker, Radio, DatePicker, Tooltip, Space } from 'antd';
import { MinusOutlined } from '@ant-design/icons';
import moment from 'moment';
import { Formik } from 'formik';
const { Option } = Select;
let id;
let formik;
function GetPulseForm() {
  return (
    <React.Fragment>
      <Form.Item
        label="need to be"
        rules={[{ required: true, }]}
      >
        <Select 
        onChange={(value) => { formik.setFieldValue("pulse_opertaor" + id,value)}}
        value = {formik.values["pulse_opertaor"+ id]}               
        style={{ width: "80px" }} 
        >
          <Option value=">">{'>'}</Option>
          <Option value="=">{'='}</Option>
          <Option value="<">{'<'}</Option>
        </Select>
      </Form.Item>
      <Form.Item rules={[{ type: 'number', min: 0, max: 150 }]}
        label="from"
      >
        <InputNumber
                        onChange={(value) => { formik.setFieldValue("pulse_value" + id,value)}}
                        value = {formik.values["pulse_value"+ id]}               
        
        />
      </Form.Item>
    </React.Fragment>

  )
}
function GetLocationForm() {
  const [locationSelect, setlocationSelect] = useState(false)
  return (
    <React.Fragment>
      <Form.Item
        label="Select a location"
        rules={[{ required: true, }]}
      >
        <Select
          style={{ width: "100px" }}
          value = {formik.values["location_value"+ id]}         
          onChange={(value) => { 
            formik.setFieldValue("location_value" + id,value);
          if (value === "loction") { setlocationSelect(true)} 
          }}>
          <Option value="Home">Home</Option>
          <Option value="Work">Work</Option>
          <Option value="loction">Locations that the person react more than</Option>
        </Select>
      </Form.Item>
      {locationSelect &&
        <Form.Item rules={[{ required: true, type: 'number', min: 1, max: 10 }]}>
          <InputNumber
                  onChange={(value) => { formik.setFieldValue("location_value_number_of_times" + id,value)}}
                  value = {formik.values["location_value_number_of_times"+ id]}                 
          />
        </Form.Item>
      }
    </React.Fragment>

  )
}
function GetTimeForm() {
  return (
    <React.Fragment>
      <Form.Item rules={[{ type: 'time', required: true }]}>
        <TimePicker 
        onChange={(value) => { formik.setFieldValue("time_value" + id,value)}}
        value = {formik.values["time_value"+ id]}       
        defaultValue={moment('00:00', 'HH:mm')} 
        format={'HH:mm'} />
      </Form.Item>
    </React.Fragment>
  )
}

function GetDayForm() {
  const [radioValue, setradioValue] = useState("Permanent");
  return (
    <React.Fragment>
      <Form.Item
        label="Select Day"
        rules={[{ required: true, }]}>
        <Select 
          id={"day_value" + id}
          name={"day_value" + id}   
          onChange={(value) => { formik.setFieldValue("day_value" + id,value)}}
          value = {formik.values["day_value"+ id]}     
          defaultValue = {(formik.values["day_value"+ id] !=null) ? formik.values["day_value"+ id] : "1"}  
          style={{ width: "100px" }}>
          <Option value="1">Sunday</Option>
          <Option value="2">Monday</Option>
          <Option value="3">Thuesday</Option>
          <Option value="4">Wednesday</Option>
          <Option value="5">Thursday</Option>
          <Option value="6">Friday</Option>
          <Option value="7">Saturday</Option>
        </Select>
      </Form.Item>
      {/* <Radio.Group 
      value={radioValue}
      onChange={(e) => { setradioValue(e.target.value);formik.setFieldValue("selected_by_user_option" + id,(e.target.value === "Custom")) }}>
        <Radio value={"Custom"}>
          Custom-selected by the user
      </Radio>
        <Radio value={"Permanent"}>
          Pre-selected by the creator
      </Radio>
      </Radio.Group>
      {(radioValue === "Permanent") && */}
        <Form.Item rules={[{ type: 'time', required: true }]}>
          <TimePicker 
          id={"time_creator_selected" + id}
          name={"time_creator_selected" + id}    
          onChange={(date, dateString) => {formik.setFieldValue("time_creator_selected" + id,moment(dateString, 'HH:mm'))}}    
          value={formik.values["time_creator_selected" + id]}          
          defaultValue={moment('00:00', 'HH:mm')} 
          format={'HH:mm'} 
          showNow={false} />
        </Form.Item>
      {/* } */}

    </React.Fragment>
  )
}

function GetDateForm() {
  return (
    <React.Fragment>
      <Form.Item rules={[{ type: 'date', required: true }]}>
        <DatePicker 
          id={"date_value" + id}
          name={"date_value" + id}
          onChange={(date, dateString) => {formik.setFieldValue("date_value" + id,moment(dateString, 'YYYY-MM-DD'))}}
          value={formik.values["date_value" + id]}
        />
      </Form.Item>
      <Form.Item rules={[{ type: 'time', required: true }]}>
        <TimePicker 
        id={"time_value" + id}
        name={"time_value" + id}    
        onChange={(date, dateString) => {formik.setFieldValue("time_value" + id,moment(dateString, 'HH:mm'))}}    
        value={formik.values["time_value" + id]}
        defaultValue={moment('00:00', 'HH:mm')} 
        format={'HH:mm'} 
        showNow={false} />
      </Form.Item>
    </React.Fragment>
  )
}


function OneMeasureSelect(props) {
  id = props.id;
  formik = props.formik;
  const [selectedMeasure, setselectedMeasure] = useState("");
  const dictFunctionSelect = {
    "": () => { },
    "pulse": GetPulseForm(),
    "location": GetLocationForm(),
    "Time": GetTimeForm(),
    "day": GetDayForm(),
    "date": GetDateForm(),
  }
  useEffect(() => {
    (formik.values["measure_name" + id] != null) ? setselectedMeasure(formik.values["measure_name" + id]) : setselectedMeasure("");
  }, [])

  return (
    <React.Fragment>
      <Space align="start">
        <Tooltip title="delete measure" placement="left">
          <Button shape="circle" icon={<MinusOutlined />}
            onClick={() => { props.delMeasure(props.id) }}>
          </Button>
        </Tooltip>
        <Form.Item
          rules={[
            {
              required: true,
            },
          ]}
        >
          <Select className="m1"
            id={"measure_name" + id}
            name={"measure_name" + id}          
            placeholder="Select a measure"
            style={{ width: "150px" }}
            onChange={(value) => { setselectedMeasure(value); formik.setFieldValue("measure_name" + id,value)}}
            value = {formik.values["measure_name"+ id]}
          >
            {/* <Option value="pulse">Pulse</Option> */}
            {/* <Option value="location">Location</Option> */}
            <Option value="Time">Time (everyDay)</Option>
            <Option value="day">day</Option>
            <Option value="date">date</Option>
            {/* <Option value="Excessive_frenzy">Excessive frenzy</Option> */}
          </Select>
        </Form.Item>
        {dictFunctionSelect[selectedMeasure]}
      </Space>
    </React.Fragment>
  )
}

OneMeasureSelect.propTypes = {

}

export default OneMeasureSelect

