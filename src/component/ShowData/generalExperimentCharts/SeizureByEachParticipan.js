import React,{useEffect,useState} from 'react'
import { Descriptions, Badge } from 'antd';
import {  XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer,Bar,BarChart } from 'recharts';
import { Row,Col } from "antd";


function getSeizureByMonth(seizureListByParticipantDict){
    let monthCountArr = [0,0,0,0,0,0,0,0,0,0,0,0]
    const monthNameArr = ["January", "February", "March", "April","May", "June", "July", "August", "September", "October", "November", "December"];

    // const participantsList = await getParticipantsList(expKey);
    // for(const participant of participantsList) {
    //         const seizureDateList =  await getSeizureDateListByParticipant(expKey, participant);
    //         seizureDateList.forEach(seizure => {
    //                 monthCountArr[seizure.date.getMonth()]++;
    //             });    
    //     }
    Object.keys(seizureListByParticipantDict).forEach(key => {
        seizureListByParticipantDict[key].forEach(seizure => {
            monthCountArr[seizure.data.date.toDate().getMonth()]++;
        })
    })
        
    const data = [...Array(12).keys()].map(monthIndex => {
        return {
            monthName: monthNameArr[monthIndex],
            "seizure count": monthCountArr[monthIndex] 
        }
    })
    return data;
}

function getAllSeizureCount(dataSeizureByMonth){
    let counter = 0;
    dataSeizureByMonth.forEach(element => {
        counter += element["seizure count"];
    })
    return counter;
}

function getSeizureCountByParticipant(seizureListByParticipantDict){
    if(seizureListByParticipantDict == null) return;
    const data = Object.keys(seizureListByParticipantDict).map(participantId => {
        return {
            participantId: participantId,
            "seizure count": seizureListByParticipantDict[participantId].length 
        }
    });
    return data;

}

// function getChartsSeizureCountByParticipant(seizureListByParticipantDict){
//     return (
//         <BarChart width={500} height={250} data={getSeizureCountByParticipant(seizureListByParticipantDict)}   layout="vertical"
//         >
//         <CartesianGrid strokeDasharray="3 3" />
//         <XAxis type="number"/>
//         <YAxis type="category"  dataKey="participantId"/>
//         <Tooltip />
//         <Legend />
//         <Bar dataKey="seizure count" fill="#8884d8" />
//         </BarChart>
//     )
// }

function SeizureByEachParticipan(props) {
    const [dataSeizureByMonth, setdataSeizureByMonth] = useState([])
    const allSeizureCount = getAllSeizureCount(dataSeizureByMonth);

    useEffect(() => {
        if(props.seizureListByParticipantDict == null) return;
        const ret = getSeizureByMonth(props.seizureListByParticipantDict);
        setdataSeizureByMonth(ret);
    }, [props.seizureListByParticipantDict])

    return (
        <Row type="flex" justify="center" align="middle" style={{height:"70vh"}}>
            <Col style={{width: "80%"}}>
            <Row >
                <React.Fragment>
            {props.seizureListByParticipantDict != null && 
                        <ResponsiveContainer width="80%" height={400}>
                    <BarChart width={800} height={250} data={getSeizureCountByParticipant(props.seizureListByParticipantDict)}   
                    layout="vertical"
                    margin={{ top: 5, right: 0, left: 200, bottom: 5  }}
                    >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis type="number"/>
                    <YAxis type="category"  dataKey="participantId" tick={{ fontSize: 14, width: 250 }}/>
                    <Tooltip />
                    <Legend />
                    <Bar dataKey="seizure count" fill="#8884d8" />
                    </BarChart></ResponsiveContainer>}
            </React.Fragment>
            </Row>
            <Row>
        <Descriptions bordered>
                <Descriptions.Item label="how much seizure">{allSeizureCount}</Descriptions.Item>
        </Descriptions>
        </Row>
        </Col>
        </Row>
    )
}


export default SeizureByEachParticipan

