import React from 'react'
import db from '../../services/firestore.config';
import "firebase/firestore"
import firebase from 'firebase/app'


const db_data_collected = db.collection('Data_collected');


export async function getParticipantsList(expKey){

    let participantsArr = [];
      
    await db_data_collected.doc(expKey)
    .get()
    .then((doc) => {
        const data = doc.data();
        participantsArr = data.participants_id;
        // querySnapshot.forEach((doc) => {
        //     participantsArr = [...participantsArr,doc.id];
        // });
        }).catch((error) => {
        console.log("Error getting document:", error);
    });
    return participantsArr;
}

// export async function getSeizureListByParticipant (expKey, participantId) {
//     //get the action arr
//     let seizureArr = [];
//     await db_data_collected.doc(expKey).collection(participantId)
//     .get()
//     .then((querySnapshot) => {
//         seizureArr = querySnapshot.docs.map((doc) => {
//             return doc.data()
//           })    
//     }).catch((error) => {
//         console.log("Error getting document:", error);
//     });
//     return seizureArr;
// }

export async function getSeizureDateListByParticipant (expKey, participantId) {
    //get the action arr
    let seizureDateArr = [];
    await db_data_collected.doc(expKey).collection(participantId)
    .get()
    .then((querySnapshot) => {
        seizureDateArr = querySnapshot.docs.map((doc) => {
            return { id: doc.id, date: doc.data().date.toDate()};
          })    
    }).catch((error) => {
        console.log("Error getting document:", error);
    });

    seizureDateArr = seizureDateArr.filter(seizure => seizure.data.date != null);
    //sort the array
    seizureDateArr.sort(function(a,b){
        return new Date(b.date) - new Date(a.date);
      });
      
    return seizureDateArr;
}


export async function getSeizureListByParticipant (expKey, participantId) {
    //get the action arr
    let seizureDateArr = [];
    await db_data_collected.doc(expKey).collection(participantId)
    .get()
    .then((querySnapshot) => {
        seizureDateArr = querySnapshot.docs.map((doc) => {
            return { id: doc.id, data: doc.data()};
          })    
    }).catch((error) => {
        console.log("Error getting document:", error);
    });

    //sort the array
    seizureDateArr = seizureDateArr.filter(seizure => seizure.data.date != null);
    seizureDateArr.sort(function(a,b){
        return new Date(b.data.date.toDate()) - new Date(a.data.date.toDate());
      });
    return seizureDateArr;
}

export async function getSeizureDoc (expKey, participantId, seizureId){
    let seizureData = {}; 
    await db.collection('Data_collected/'+expKey+'/'+participantId).doc(seizureId)
    .get()
    .then((doc) => {
        seizureData = doc.data();
    }).catch((error) => {
        console.log("Error getting document:", error);
    });
    return seizureData;

}


export async function getSeizureListByParticipantDict(expKey){
    let dict = {}
    const participantsList = await getParticipantsList(expKey);
    for(const participant of participantsList) {
        dict[participant] =  await getSeizureListByParticipant(expKey, participant);
        }
        return dict;

}



export async function getExperimentData(expKey) {
    const db_experiments = db.collection('experiments_details');
    //get the action arr
    let new_action_arr = [];
    let exp_data = await db_experiments.doc(expKey).get();
    return exp_data.data();
}
