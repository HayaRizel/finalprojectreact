import React, {PureComponent} from 'react'
import { Descriptions, Badge } from 'antd';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer,Bar,BarChart } from 'recharts';
import { Row,Col } from "antd";



function getSeizureByMonth(seizureArray){
    let monthCountArr = [0,0,0,0,0,0,0,0,0,0,0,0]
    const monthNameArr = ["January", "February", "March", "April","May", "June", "July", "August", "September", "October", "November", "December"];
    
    seizureArray.forEach(seizure => {
        monthCountArr[seizure.data.date.toDate().getMonth()]++;
    });
    const data = [...Array(12).keys()].map(monthIndex => {
        return {
            monthName: monthNameArr[monthIndex],
            "seizure count": monthCountArr[monthIndex] 
        }
    })
    return data;
}

function SpecificParticipantSeizureByMonth(props) {
    const seizureList = props.seizureListByParticipantDict[props.participantId];
    let dataSeizureByMonth = getSeizureByMonth(seizureList);

    return (
        <Row type="flex" justify="center" align="middle" style={{height:"70vh"}}>
            <Col style={{width: "80%"}}>
            <Row>
            <ResponsiveContainer width="80%" height={400}>
            <BarChart width={950} height={250} data={dataSeizureByMonth}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="monthName" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Bar dataKey="seizure count" fill="#8884d8" />
            </BarChart>
            </ResponsiveContainer>
            </Row>
            <Row>
        <Descriptions bordered>
                <Descriptions.Item label="how much seizure">{seizureList.length}</Descriptions.Item>
        </Descriptions>
        </Row>
        </Col>
        </Row>
    )
}


export default SpecificParticipantSeizureByMonth

