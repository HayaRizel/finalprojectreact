import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Button, Card } from 'antd'
import { CheckOutlined, EditOutlined, EllipsisOutlined, UpOutlined } from '@ant-design/icons'
import RangeQuestion from './actionKinds/RangeQuestion';
import CloseQuestion from './actionKinds/CloseQuestion';
import Instruction from './actionKinds/Instruction';
import OpenQuestion from './actionKinds/OpenQuestion';
import StroopGame from './actionKinds/StroopGame';

const actionKindsArr = {
    "question_close": {
        action_name: "question_close",
        component: <CloseQuestion />,
        componentDescription: "close Question",
        componentName: "close Question",
    },
    "instruction": {
        action_name: "instruction",
        component: <Instruction />,
        componentDescription: "Instruction",
        componentName: "Instruction"
    },
    "question_open": {
        action_name: "question_open",
        component: <OpenQuestion />,
        componentDescription: "Open Question",
        componentName: "Open Question"
    },
    "question_range": {
        action_name: "question_range",
        component: <RangeQuestion />,
        componentDescription: "Range Question",
        componentName: "Range Question",
    },
    "stroop_game": {
        action_name: "stroop_game",
        component: <StroopGame />,
        componentDescription: "Stroop Game",
        componentName: "Stroop Game",
    }

}

function ModalOneAction(props) {
    const [editStatus, seteditStatus] = useState(true);

    return (
        <Card style={{ border: "solid",borderWidth: 0.01, borderColor: "gainsboro"}}
            extra={<Button onClick={() => { props.delActionCard() }}>X</Button>}
            title={<p>{actionKindsArr[props.action_name].componentName}</p>}
            //description={props.componentDescription}
            actions={[
                <EditOutlined key="edit" onClick={() => { seteditStatus(true) }} />,
                <CheckOutlined key="save" onClick={() => {seteditStatus(false) }} />,
            ]}
            bordered
        >
            {/* <Card.Meta description={props.componentDescription} /> */}
            {/* <Button>X</Button> */}
            {React.cloneElement(
                actionKindsArr[props.action_name].component,
                {
                    editstatus: editStatus,
                    expKey: props.expKey,
                    key: props.id,
                    id: props.id,
                    flow_key: props.flow_key,
                    initialValue: props.initialValue,
                }
            )}
        </Card>
    )
}

ModalOneAction.propTypes = {
    expKey: PropTypes.string,
    flow_key: PropTypes.string,
    action_name: PropTypes.string,
    delActionCard: PropTypes.func,
    id: PropTypes.string,
    initialValue: PropTypes.object,
}

export default ModalOneAction

