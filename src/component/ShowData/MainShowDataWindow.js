import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { useHistory, useParams } from 'react-router-dom'
import {
    getExperimentData,
    getParticipantsList,
    getSeizureDateListByParticipant, getSeizureListByParticipantDict
} from './getDataFromDb';
import { Menu, Button } from 'antd';
import { Layout } from 'antd';
import SeizureByEachParticipan from './generalExperimentCharts/SeizureByEachParticipan';
import SpecificParticipantSeizureByMonth from './specificParticipantCharts/SpecificParticipantSeizureByMonth';
import moment from 'moment';
import SeizureShowData from './specificSeizureCharts/SeizureShowData';
import SeizureByMonthChart from './generalExperimentCharts/SeizureByMonthChart';
import SpecificParticipantAvgStroopGame from './specificParticipantCharts/SpecificParticipantAvgStroopGame';
import SpecificParticipantRangeQuestionChart from './specificParticipantCharts/SpecificParticipantRangeQuestionChart';
import SpecificParticipantOpenQuestionChart from './specificParticipantCharts/SpecificParticipantOpenQuestionChart';
import SpecificParticipantCloseQuestionChart from './specificParticipantCharts/SpecificParticipantCloseQuestionChart';
const { SubMenu } = Menu;

const { Header, Footer, Sider, Content } = Layout;

function MainShowDataWindow(props) {
    const experimentKey = useParams().experimentKey;
    const userId = props.userId;
    const [currentParticipant, setcurrentParticipant] = useState(null);
    const [participantsList, setparticipantsList] = useState([]);
    const [seizureListByParticipantDict, setseizureListByParticipantDict] = useState(null);
    const [currentSeizure, setcurrentSeizure] = useState(null);
    const [expData, setexpData] = useState(null);
    const [expDataFeadbackArr, seteexpDataFeadbackArr] = useState([]);
    const [currentQuestionFeadbak, setcurrentQuestionFeadbak] = useState(null);
    const [currentContentComponent, setcurrentContentComponent] = useState("SeizureByEachParticipan");
    const contentComponents = {
        SeizureByEachParticipan: <SeizureByEachParticipan seizureListByParticipantDict={seizureListByParticipantDict} />,
        SeizureByMonthChart: <SeizureByMonthChart seizureListByParticipantDict={seizureListByParticipantDict} />,
        SpecificParticipantSeizureByMonth: <SpecificParticipantSeizureByMonth participantId={currentParticipant} seizureListByParticipantDict={seizureListByParticipantDict} />,
        SpecificParticipantAvgStroopGame: <SpecificParticipantAvgStroopGame participantId={currentParticipant} seizureListByParticipantDict={seizureListByParticipantDict} />,
        SeizureShowData: <SeizureShowData participantId={currentParticipant} seizureId={currentSeizure} seizureListByParticipantDict={seizureListByParticipantDict} />,
        SpecificParticipantRangeQuestionChart: <SpecificParticipantRangeQuestionChart question_obj={currentQuestionFeadbak}  participantId={currentParticipant}   seizureListByParticipantDict={seizureListByParticipantDict} />,
        SpecificParticipantOpenQuestionChart: <SpecificParticipantOpenQuestionChart question_obj={currentQuestionFeadbak}  participantId={currentParticipant}   seizureListByParticipantDict={seizureListByParticipantDict} />,
        SpecificParticipantCloseQuestionChart: <SpecificParticipantCloseQuestionChart question_obj={currentQuestionFeadbak}  participantId={currentParticipant}   seizureListByParticipantDict={seizureListByParticipantDict} />,
    }


    useEffect(() => {
        getParticipantsList(experimentKey)
            .then(ret => {
                setparticipantsList(ret);
            }).catch((error) => {
                console.log("error getting parcipants list: ", error);
            });
            getSeizureListByParticipantDict(experimentKey)
            .then(ret => {
                setseizureListByParticipantDict(ret);
            }).catch((error) => {
                console.log("error getting parcipants list: ", error);
            });
            getExperimentData(experimentKey)
            .then(expData => {
                setexpData(expData);
                if(expData != null || expData.app_flow != null){ 
                    const feadback = expData.app_flow.find(x => x.flow_key === "feadback")
                    if(feadback != null && feadback.action_arr != null)
                    seteexpDataFeadbackArr(feadback.action_arr);
                }
            }).catch((error) => {
                console.log("error getting parcipants list: ", error);
            });

    }, [experimentKey])


    const handleParticipantsMenuClick = e => {
        //general key replace
        const participantKey = e.keyPath.find(k => k.startsWith("participantId"));
        const seizureId = e.keyPath.find(k => k.startsWith("seizureId"));
        const questionFeadbakId = e.keyPath.find(k => k.startsWith("questionFeadbakId"));

        if (participantKey != null) {
            setcurrentParticipant(participantKey.split('/')[1]);
        }
        if (seizureId != null) {
            setcurrentSeizure(seizureId.split('/')[1]);
        }
        if (questionFeadbakId != null) {
            setcurrentQuestionFeadbak(expDataFeadbackArr.find(q => q.action_value.question_text === questionFeadbakId.split('/')[1]));
        }

        //content replace
        Object.keys(contentComponents).forEach(contentComp => {
            if (e.key.startsWith(contentComp)) {
                setcurrentContentComponent(contentComp);
            }
    
        })
        // if (e.key.startsWith("SpecificParticipantSeizureByMonth")) {
        //     setcurrentContentComponent("SpecificParticipantSeizureByMonth");
        // }
        // if(e.key.startsWith("SpecificParticipantAvgStroopGame")){
        //     setcurrentContentComponent("SpecificParticipantAvgStroopGame");

        // }
        // if (e.key.startsWith("SeizureByMonthChart")) {
        //     setcurrentContentComponent("SeizureByMonthChart");
        // }

        if (e.key.startsWith("seizureId")) {
            setcurrentContentComponent("SeizureShowData");
        }
        if (e.key.startsWith("questionFeadbakIdRange")) {
            setcurrentContentComponent("SpecificParticipantRangeQuestionChart");
        }
        if (e.key.startsWith("questionFeadbakIdOpen")) {
            setcurrentContentComponent("SpecificParticipantOpenQuestionChart");
        }
        if (e.key.startsWith("questionFeadbakIdClose")) {
            setcurrentContentComponent("SpecificParticipantCloseQuestionChart");
        }


        // if (e.key.startsWith("SeizureByEachParticipan")) {
        //     setcurrentContentComponent("SeizureByEachParticipan");
        // }

    };

    //only one participant can be open
    const rootSubmenuKeys = participantsList.map(p => "participantId/" + p);
    const [openKeys, setOpenKeys] = useState([]);
    const onOpenChange = keys => {
        const latestOpenKey = keys.find(key => openKeys.indexOf(key) === -1);
        if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
            setOpenKeys(keys);
        } else {
            setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
        }
    };


    return (
        <div>
            <Layout>
                <Sider
                    width={300}
                    style={{ minHeight: '100vh' }}>
                    <Menu
                        onClick={handleParticipantsMenuClick}
                        openKeys={openKeys} onOpenChange={onOpenChange}
                        style={{ width: 300 }}
                        mode="inline"
                        theme="light"
                    >
                        <Menu.ItemGroup key={"generalExperimentData"} title={"general experiment data"}>
                            <Menu.Item key={"SeizureByEachParticipan"}>Seizure By Each Participant Chart</Menu.Item>
                            <Menu.Item key={"SeizureByMonthChart"}>Seizure By Month Chart</Menu.Item>

                        </Menu.ItemGroup>

                        <Menu.ItemGroup key={"participantList"} title={"participant list"}>
                            {participantsList.map(participantName =>
                                <SubMenu key={"participantId/" + participantName} title={participantName}
                                //   icon={<PieChartOutlined />}
                                >
                                    <Menu.ItemGroup key={"generalData" + participantName} title={"General Data"}>
                                        <Menu.Item key={"SpecificParticipantSeizureByMonth/" + participantName}
                                        >
                                            Seizure By Month Chart
                                        </Menu.Item>
                                        {seizureListByParticipantDict != null
                                        && seizureListByParticipantDict[participantName] != null 
                                        && seizureListByParticipantDict[participantName]
                                        && seizureListByParticipantDict[participantName][0].data != null 
                                        && seizureListByParticipantDict[participantName][0].data.app_info != null
                                        && seizureListByParticipantDict[participantName][0].data.app_info.stroopGame != null
                                        && <Menu.Item key={"SpecificParticipantAvgStroopGame/" + participantName}
                                        >
                                            stroop game avg time
                                        </Menu.Item>}

                                        <SubMenu key={"SpecificParticipantRangeQuestionChart/" + participantName} title="Feedback Question Graphs">
                                            {expDataFeadbackArr.map(feadbackQuestion =><React.Fragment>
                                                {feadbackQuestion.action_name === "question_range" &&
                                                    <Menu.Item key={"questionFeadbakIdRange/" + feadbackQuestion.action_value.question_text}>
                                                        {"Range Question " + feadbackQuestion.action_value.question_text}
                                                    </Menu.Item>
                                                }
                                                {feadbackQuestion.action_name === "question_open" &&
                                                    <Menu.Item key={"questionFeadbakIdOpen/" + feadbackQuestion.action_value.question_text}>
                                                        {"Open Question " + feadbackQuestion.action_value.question_text}
                                                    </Menu.Item>
                                                }
                                                {feadbackQuestion.action_name === "question_close" &&
                                                    <Menu.Item key={"questionFeadbakIdClose/" + feadbackQuestion.action_value.question_text}>
                                                        {"Close Question " + feadbackQuestion.action_value.question_text}
                                                    </Menu.Item>
                                                }

                                            </React.Fragment>)}
                                        </SubMenu>



                                        
                                    </Menu.ItemGroup>

                                    <Menu.ItemGroup key={participantName} title="seizure list">
                                        {seizureListByParticipantDict != null && seizureListByParticipantDict[participantName] != null && seizureListByParticipantDict[participantName].map(seizure =>
                                            <Menu.Item key={"seizureId/" + seizure.id}
                                            >
                                                {moment(seizure.data.date.toDate()).format('DD/MM/YYYY hh:mm')}
                                            </Menu.Item>
                                        )}
                                    </Menu.ItemGroup>
                                </SubMenu >
                            )}
                        </Menu.ItemGroup>

                    </Menu>
                </Sider>
                <Content style={{ minHeight: '100vh', margin: '0 16px' }}>
                    {contentComponents[currentContentComponent]}
                </Content>
            </Layout>
        </div>
    )
}

MainShowDataWindow.propTypes = {

}

export default MainShowDataWindow

