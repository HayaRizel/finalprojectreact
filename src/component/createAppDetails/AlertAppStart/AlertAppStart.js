import React, { useState,useEffect } from 'react'
import { Form, Input, Button, Checkbox, Select, Row, Col, Tooltip, Space } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { v4 as uuid } from 'uuid';
import OneMeasureSelect from './OneMeasureSelect';
import { useFormik } from 'formik';
import {db_experiments} from '../../../services/firestore.config';
import Operation from 'antd/lib/transfer/operation';
import moment from 'moment';

const { Option } = Select;

const measure_param_arr = {
  "pulse": ["pulse_opertaor","pulse_value"],
  "location":["location_value","location_value_number_of_times"],
  "Time": ["time_value"],
  "day": ["day_value"/*,"selected_by_user_option"*/,"time_creator_selected"],
  "date": ["date_value","time_value"],
  "Excessive_frenzy":["measure_name"],
}

function AlertAppStart(props) {
  const [measueArr, setmeasueArr] = useState([]);
  const [enterApp, setenterApp] = useState(true);
  const [appAlert, setappAlert] = useState(false);
  const delMeasure = (id) => {
    if (measueArr.length === 1) return;
    setmeasueArr(measueArr.filter(x => x !== id))
  }
  const formik = useFormik({
    initialValues: {
        app_alert_option: true,
        enter_app_option: false,
    },
    onSubmit:  (values) => {
        const arr = measueArr.map(measureId => {
          if(!measueArr.includes(measureId)) return;
            if(values["measure_name" + measureId] == null) return;
            let measureObj = {};
            measureObj.measure_name = values["measure_name" + measureId];
            let measure_value = {};
            measure_param_arr[measureObj.measure_name].forEach(param => {
              if(values[param + measureId] == null){
                  return;
              }


                measure_value[param] = values[param + measureId];
                if(param === "date_value") measure_value[param] = values[param + measureId].format('DD/MM/YYYY');
                if(param === "time_value" || param === "time_creator_selected") measure_value[param] = values[param + measureId].format('HH:mm');
  
            })   
            measureObj.measure_value = measure_value;
            measureObj.measure_id = measureId;
            return measureObj;   
        });
        //delete all the empty measure from the array - maybe not need the filter
        const measuresArr_final = arr.filter(node => node != null)
        db_experiments.doc(props.expKey)
            .set({
                notifications: {
                  app_alert_option: values.app_alert_option,
                  enter_app_option: values.enter_app_option,
                  measures_alert: measuresArr_final,
                }
            }, { merge: true })
            .then(ret => {
                console.log(ret);
                props.onSubmit();
            })
            .catch(err => {
                console.error(err)
            });
    }
});

useEffect(() => {
  db_experiments.doc(props.expKey)
          .get()
          .then(ret => {
            if(ret.data().notifications == null || ret.data().notifications.measures_alert == null){
              setmeasueArr([uuid()]);
              return;
            } 
            setenterApp(ret.data().notifications.enter_app_option);
            setappAlert(ret.data().notifications.app_alert_option);

              const measures_arr_from_db = ret.data().notifications.measures_alert;
              let initialMeasureArr = []
              measures_arr_from_db.forEach(measureObj => {
                      const measureId = measureObj.measure_id;
                      initialMeasureArr = [...initialMeasureArr,measureObj.measure_id];
                      // formik.setFieldValue("time_"+measure.measure_name, moment(measure.how_much_time, 'HH:mm:ss'))
                      // formik.setFieldValue("freq_"+measure.measure_name, moment(measure.frequency, 'HH:mm:ss'))   
                      formik.setFieldValue("measure_name" + measureObj.measure_id, measureObj.measure_name); 
                      measure_param_arr[measureObj.measure_name].forEach(param => {
                        if(param === "time_value" || param === "time_creator_selected" || param === "date_value"){
                          if(param === "date_value") {
                            const d = moment(measureObj.measure_value[param], 'DD/MM/YYYY');
                            const goodD = d.format('DD-MM-YYYY')
                            formik.setFieldValue(param + measureId,moment(goodD, 'YYYY-MM-DD'));
                          }
                          if(param === "time_value" || param === "time_creator_selected") formik.setFieldValue(param + measureId,moment(measureObj.measure_value[param], 'HH:mm'));    
                        }else{
                          formik.setFieldValue(param + measureObj.measure_id, measureObj.measure_value[param]);
                        }
                      });
                });
                setmeasueArr(initialMeasureArr);
              console.log(ret);
          })
          .catch(err => {
              console.error(err)
          });
}, [])


  return (
    <div>
      <Form onFinish={formik.handleSubmit}>
        <Row>
          <Form.Item name="enter_the_app" >
            <Checkbox checked={enterApp} onChange={() => { setenterApp(!enterApp) }}>
              The user need to enter the app and alert
            </Checkbox>
          </Form.Item>
        </Row>
        <Row>
          <Form.Item name="app_alert" >
            <Checkbox checked={appAlert} onChange={() => { setappAlert(!appAlert) }}>
              The app should notifications the user when:
            </Checkbox>
          </Form.Item>
          {(appAlert === true) &&
            <React.Fragment >
              <Space align="start">
                <Col>
                  <Tooltip title="add new measure">
                    <Button shape="circle" icon={<PlusOutlined />}
                      onClick={() => { setmeasueArr([...measueArr, uuid()]) }}>
                    </Button>
                  </Tooltip>
                </Col>
                <Col>
                  {measueArr.map(id =>
                    <Row key={id}>
                      <OneMeasureSelect id={id} delMeasure={delMeasure} formik={formik} selectedMeasure={formik.values["measure_name" + id]}/>
                    </Row>
                  )}
                  <Row>
                    <Button onClick={() => { setmeasueArr([uuid()]) }}>
                      Reset All
                  </Button>
                  </Row>
                </Col>
              </Space>
            </React.Fragment>
          }
        </Row>
        <Button type="primary" htmlType="submit">save</Button>
      </Form>
    </div>
  )
}

export default AlertAppStart
