import React from 'react'
import db from '../../../services/firestore.config';
import "firebase/firestore"
import firebase from 'firebase/app'


export default function addActionToDb(action_obj,flow_key,expKey) {
    const db_experiments = db.collection('experiments_details');
    //get the action arr
    let new_action_arr = [];
    db_experiments.doc(expKey)
    .get()
    .then((doc) => {
        if (doc.exists) {
            let app_flow;
            (doc.data().app_flow != null) ? app_flow =  doc.data().app_flow : app_flow = [];
            const flow = app_flow.find(x => x.flow_key === flow_key)
            if (flow == null){
                console.log("error - the flow not exists");
            }
            let action_arr = flow.action_arr;
            if(action_arr != null){
                action_arr = action_arr.filter(x => x.action_id !== action_obj.action_id)
            }
            flow.action_arr = [...action_arr,action_obj];
                    //add the new
        db_experiments.doc(expKey)
        .update({
            // action: firebase.firestore.FieldValue.arrayUnion(action_obj),
            app_flow: app_flow,
        }, { merge: true })
        .then(ret => {
            console.log(ret);
        })
        .catch(err => {
            console.error(err)
        });

        }
    }).catch((error) => {
        console.log("Error getting document:", error);
    });
}

export function deleteActionFromDb(action_obj_id,flow_key,expKey){
    const db_experiments = db.collection('experiments_details');
    //get the action arr
    let new_action_arr = [];
    db_experiments.doc(expKey)
    .get()
    .then((doc) => {
        if (doc.exists) {
            let app_flow;
            (doc.data().app_flow != null) ? app_flow =  doc.data().app_flow : app_flow = [];
            const flow = app_flow.find(x => x.flow_key === flow_key)
            if (flow == null){
                console.log("error - the flow not exists");
            }
            const action_arr = flow.action_arr
            if(action_arr != null){
                new_action_arr = action_arr.filter(x => x.action_id !== action_obj_id)
            }
            flow.action_arr = new_action_arr;
        //update the action array
        db_experiments.doc(expKey)
        .update({
            app_flow: app_flow,
        }, { merge: true })
        .then(ret => {
            console.log(ret);
        })
        .catch(err => {
            console.error(err)
        });

        }
    }).catch((error) => {
        console.log("Error getting document:", error);
    });

}


export function addFlowToDb(flow_obj,expKey) {
    const db_experiments = db.collection('experiments_details');
    //get the action arr
    let new_action_arr = [];
    db_experiments.doc(expKey)
    .get()
    .then((doc) => {
        if (doc.exists) {
            let app_flow;
            (doc.data().app_flow != null) ? app_flow =  doc.data().app_flow : app_flow = [];
            const flow = app_flow.find(x => x.flow_key === flow_obj.flow_key)
            if (flow != null){
                console.log("flow allready exist");
                return;
            }    
            app_flow = [...app_flow,flow_obj]                
            //add the new
        db_experiments.doc(expKey)
        .update({
            // action: firebase.firestore.FieldValue.arrayUnion(action_obj),
            app_flow: app_flow,
        }, { merge: true })
        .then(ret => {
            console.log(ret);
        })
        .catch(err => {
            console.error(err)
        });

        }
    }).catch((error) => {
        console.log("Error getting document:", error);
    });
}


export function removeFlowFromDb(flow_key,expKey) {
    const db_experiments = db.collection('experiments_details');
    //get the action arr
    let new_action_arr = [];
    db_experiments.doc(expKey)
    .get()
    .then((doc) => {
        if (doc.exists) {
            let app_flow;
            (doc.data().app_flow != null) ? app_flow =  doc.data().app_flow : app_flow = [];
            app_flow = app_flow.filter(x => x.flow_key !== flow_key)
            //add the new
        db_experiments.doc(expKey)
        .update({
            // action: firebase.firestore.FieldValue.arrayUnion(action_obj),
            app_flow: app_flow,
        }, { merge: true })
        .then(ret => {
            console.log(ret);
        })
        .catch(err => {
            console.error(err)
        });

        }
    }).catch((error) => {
        console.log("Error getting document:", error);
    });
}



export function changeFlowNameInDb(flow_obj,expKey, newName) {
    const db_experiments = db.collection('experiments_details');
    //get the action arr
    let new_action_arr = [];
    db_experiments.doc(expKey)
    .get()
    .then((doc) => {
        if (doc.exists) {
            let app_flow;
            (doc.data().app_flow != null) ? app_flow =  doc.data().app_flow : app_flow = [];
            const flow = app_flow.find(x => x.flow_key === flow_obj.flow_key)
            if (flow == null){
                console.log("error - flow dont exist");
                return;
            }   
            flow.flow_name = newName;
            //update the db
        db_experiments.doc(expKey)
        .update({
            // action: firebase.firestore.FieldValue.arrayUnion(action_obj),
            app_flow: app_flow,
        }, { merge: true })
        .then(ret => {
            console.log(ret);
        })
        .catch(err => {
            console.error(err)
        });

        }
    }).catch((error) => {
        console.log("Error getting document:", error);
    });
}


export function changeFlowHowMuchSelectInDb(flow_obj,expKey, newHowMuchSelect) {
    const db_experiments = db.collection('experiments_details');
    //get the action arr
    let new_action_arr = [];
    db_experiments.doc(expKey)
    .get()
    .then((doc) => {
        if (doc.exists) {
            let app_flow;
            (doc.data().app_flow != null) ? app_flow =  doc.data().app_flow : app_flow = [];
            const flow = app_flow.find(x => x.flow_key === flow_obj.flow_key)
            if (flow == null){
                console.log("error - flow dont exist");
                return;
            }   
            flow.how_much_select = newHowMuchSelect;
            //update the db
        db_experiments.doc(expKey)
        .update({
            // action: firebase.firestore.FieldValue.arrayUnion(action_obj),
            app_flow: app_flow,
        }, { merge: true })
        .then(ret => {
            console.log(ret);
        })
        .catch(err => {
            console.error(err)
        });

        }
    }).catch((error) => {
        console.log("Error getting document:", error);
    });
}
