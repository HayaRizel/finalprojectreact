import React,{useEffect} from 'react'
import PropTypes from 'prop-types'
import { Form, Input, Button, Tooltip, Row, Checkbox, Space, InputNumber } from 'antd'
import { useFormik } from 'formik'
import addActionToDb from '../addActionToDb'

function StroopGame(props) {
    const formik = useFormik({
        initialValues: {
            num_of_repit: 1,
        },
        onSubmit: (values) => {
            //get
            const instruction_obj = {
                "action_name": "stroop_game",
                "action_id": props.id,
                "action_value": {
                    "num_of_repit": values.num_of_repit,
                }
            }
            addActionToDb(instruction_obj, props.flow_key, props.expKey)
        }
    });

    useEffect(() => {
        const value = props.initialValue;
        if (value == null) return;
        formik.setFieldValue("num_of_repit", value.num_of_repit);
    }, [])

    useEffect(() => {
        if (props.editstatus === false) {
            formik.handleSubmit();
        }
    }, [props.editstatus])


    return (
        <Form onFinish={formik.handleSubmit}>
            <Row>
                <Form.Item
                    label="num of repit">
                    <InputNumber
                        onChange={(value) => { formik.setFieldValue("num_of_repit", value) }}
                        value={formik.values.num_of_repit}
                        disabled={!props.editstatus} />
                </Form.Item>
            </Row>
        </Form>
    )
}

StroopGame.propTypes = {

}

export default StroopGame

