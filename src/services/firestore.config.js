import firebase from 'firebase/app'
import "firebase/firestore"

// const firebaseConfig = {
//   apiKey: process.env.REACT_APP_FIREBASE_APIKEY,
//   authDomain: process.env.REACT_APP_FIREBASE_AUTHDOMAIN,
//   projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
//   storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
//   messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
//   appId: process.env.REACT_APP_MESSAGING_APP_ID,
//   measurementId: process.env.REACT_APP_FIREBASE_MEASUREMENT_ID
// };
const firebaseConfig = {
  apiKey: "AIzaSyDL7w0JJwfesgj-C19chQ84Gq-EeIkXI-I",
  authDomain: "finalproject-ccaee.firebaseapp.com",
  projectId: "finalproject-ccaee",
  storageBucket: "finalproject-ccaee.appspot.com",
  messagingSenderId: "225327862525",
  appId: "1:225327862525:web:15d30fd92cdee66e0d2494",
  measurementId: "G-R2SFZJTWVK"
};


firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();
const db_experiments = db.collection('experiments_details');

export default db;

export {db_experiments};
//https://console.firebase.google.com/u/0/project/finalproject-ccaee/firestore/data~2Fapp_details~2FiPSln1Wn2QBfiUl8B5mO


// const firebaseStorage = firebase.storage().ref()
// export {firebaseStorage};