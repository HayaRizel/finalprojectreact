import React, {useState,useEffect} from 'react'
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer, Bar, BarChart } from 'recharts';
import moment from 'moment';
import { Row,Col } from "antd";

function SpecificParticipantRangeQuestionChart(props) {
    let question_obj = props.question_obj;
    // question_obj =  {
    //     action_id: "0fd100cb-378a-440f-b295-2644eb5f7c04",
    //     action_name: "question_range",
    //     action_value: {
    //         question_text: "How distressed are you feeling now?",
    //         answer: {
    //             max: 10,
    //             min: 0,
    //             step: 1
    //         }
    //     }
    // }
    const question_text = question_obj.action_value.question_text;
    const [graphData, setgraphData] = useState([])
    useEffect(() => {
        if(props.seizureListByParticipantDict == null || props.participantId == null || props.seizureListByParticipantDict[props.participantId] == null) return;

        const seizureList = props.seizureListByParticipantDict[props.participantId];
        let data = seizureList.map(seizure => {
            if(seizure.data.feedback_question == null || seizure.data.feedback_question[question_text] == null) return;
            return {
            "date": moment(seizure.data.date.toDate()).format('DD/MM/YY hh:mm'),
            "answer": parseInt(seizure.data.feedback_question[question_text]),
        }});
        data = data.filter(x => x!= null);

            //sort the array
        data.sort(function(a,b){
        return new Date(b.date) - new Date(a.date);
      });

        setgraphData(data);
    }, [props.seizureListByParticipantDict,props.participantId,props.question_obj])

    return (
        <div>
            <Row type="flex" justify="center" align="middle" style={{height:"70vh"}}>
            <ResponsiveContainer width="80%" height={400}>
            <LineChart width={1200} height={250} data={graphData}
                margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="date" />
                <YAxis dataKey="answer"/>
                <Tooltip />
                <Legend />
                <Line type="monotone" dataKey="answer" stroke="#8884d8" />
            </LineChart>
            </ResponsiveContainer>
            </Row>

        </div>
    )
}

SpecificParticipantRangeQuestionChart.propTypes = {

}

export default SpecificParticipantRangeQuestionChart

