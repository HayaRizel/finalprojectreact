import React, {useState,useEffect} from 'react'
import PropTypes from 'prop-types'
import { Form, Input, Button, Tooltip, Row, Checkbox, Space } from 'antd';
import { useFormik } from 'formik';
import addActionToDb from '../addActionToDb';



function OpenQuestion(props) {
    const formik = useFormik({
        initialValues: {
            question_text: "",
            save_answer_option: false
        },
        onSubmit:  (values) => {
            //get
            const instruction_obj = {
                "action_name": "question_open",
                "action_id": props.id,
                "action_value": {
                    "question_text": values.question_text,
                    "answer": {
                        "save_answer_option": values.save_answer_option,
                    }
                }
            }
            addActionToDb(instruction_obj,props.flow_key,props.expKey)
        }
    });

    useEffect(() => {
        const value = props.initialValue;
        if(value == null) return;

        formik.setFieldValue("question_text",value.question_text);
        formik.setFieldValue("save_answer_option",value.answer.save_answer_option);
    }, [])


    useEffect(() => {
        if(props.editstatus === false){
            formik.handleSubmit();
        }
    }, [props.editstatus])

    return (
        <div>
            <Form onFinish={formik.handleSubmit}>
                <Row>
                    <Form.Item
                        label="write your question">
                        <Input.TextArea 
                        id = "question_text"
                        onChange = {formik.handleChange}
                        value = {formik.values.question_text}                                        
                        rows={4} 
                        disabled={!props.editstatus} />
                    </Form.Item>

                </Row>
                <Row>
                    <p>more options:</p>
                </Row>
                <Row>
                    <Checkbox 
                    checked={formik.values.save_answer_option}
                    onChange = {(e) => {formik.setFieldValue("save_answer_option",e.target.checked)}}
                    disabled={!props.editstatus}>Give user option to save the answer</Checkbox>
                </Row>
                </Form>
        </div>
    )
}

OpenQuestion.propTypes = {

}

export default OpenQuestion

