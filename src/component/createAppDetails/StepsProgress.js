import React, { useState } from 'react'
import { Steps } from 'antd';

const { Step } = Steps;

function StepProgress(props) {
    const [current, setcurrent] = useState(0);
    const onChange = (current) => {
        setcurrent(current);
        props.getCurrentStep(current);
        if(current === 2 || current === 3){
            props.onChangeStep(current);
        }
    };

    return (
        <Steps
            type="navigation"
            size="small"
            current={current}
            onChange={onChange}
            className="site-navigation-steps"
        >
            {/* <Step
                title="Step 1"
                //   subTitle="00:00:05"
                status= {props.completeStep.includes(1) ? "finish" : "wait"}
                description="choose how the app will start to react."
            /> */}
            <Step
                title="Step 2"
                status= {props.completeStep.includes(1) ? "finish" : "wait"}
                description="select what the app will show after Seizure"
            />
            <Step
                title="Step 3"
                status= {props.completeStep.includes(2) ? "finish" : "wait"}
                description="select feedback question for more data from the user"
            />
            {/* <Step
                title="Step 4"
                status= {props.completeStep.includes(4) ? "finish" : "wait"}
                description="select witch measure to measure"
            /> */}
            <Step
                title="end"
                status= {props.completeStep.includes(3) ? "finish" : "wait"}
                description="submit your experiment"
            />

        </Steps>
    )
}

export default StepProgress
