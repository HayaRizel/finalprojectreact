import React, { useState, useEffect } from 'react'
import {  Tooltip,  Col, Row, Empty } from 'antd';
import { CloseOutlined, InfoOutlined, EditOutlined } from '@ant-design/icons';
import { Card } from 'antd';
import { PlusSquareOutlined } from '@ant-design/icons';




function CardShowExperiments(props) {

  const getResearchCard = (expKey, expName, expCoverImage ,expDescription) => {
    if(expDescription == null) expDescription = "no description";
    return (
      <Tooltip 
      title={<span style={{"white-space": "pre-line"}}>{"description: \n" + expDescription}</span>
      } 
      mouseEnterDelay={0.4}>
    <Card onMouseOver={() => { }}
      style={{ width: 300 }}
      cover={
        <img
          style={{ width: 300, height: 150, opacity: 0.7 }}
          alt="experiment img"
          // src={getRandomcoverExperimentImage()}
          src ={expCoverImage}
        />
      }
      actions={[
        <InfoOutlined key="info" onClick={() => { window.location.href = '/researchData/' + expKey; }} />,
        <EditOutlined key="edit" onClick={() => { window.location.href = '/editExperiment/' + expKey; }} />,
        <CloseOutlined key="delete" onClick={props.deleteExperiment.bind(this, expKey)} />,
      ]}
    >
      <Card.Meta
        title={expName}
        // description={expDescription}
      />
    </Card>
    </Tooltip>)
  }
  const getAddNewExpCard = () => {
    return (<Card
      style={{ width: 300 }}
      cover={
        <Row type="flex" justify="center" align="middle"
          style={{ width: 300, height: 150, opacity: 0.5,border: "solid", borderColor: "Scrollbar", borderWidth: "0.1px"}}
        >
                <Tooltip 
      title= "add new experiment"
      mouseEnterDelay={0.4}>

          <Empty description={false} />
          {/* <PlusSquareOutlined style={{ fontSize : '5em'}}/> */}
          </Tooltip>
        </Row>
      }
      actions={[
        <PlusSquareOutlined key="info" onClick={() => { props.addNewResearch(); }} />,
      ]}

    >

      <Card.Meta
        title="add new experiment"
      //   description={expDescription}
      />
    </Card>)

  }
  const experimentCardList = props.expList.map(exp =>  <Col >{getResearchCard(
    exp.experimentKey, 
    exp.metadata.experiment_name,
    exp.metadata.cover_image,
    exp.metadata.description)}</Col>);

  return (
    <div>
      <Row type="flex" justify="center" align="middle">
        <Row type="flex" justify="center" align="middle" gutter={10} style={{ width: 1200 }}>
          {getAddNewExpCard()}
          {experimentCardList}

        </Row>
      </Row>
    </div>
  )
}

CardShowExperiments.propTypes = {

}

export default CardShowExperiments

