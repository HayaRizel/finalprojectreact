import React, { useState,useEffect } from 'react'
import { v4 as uuid } from 'uuid'
import ModalOneAction from './ModalOneAction'
import {  Button,  Select, Row,  Tooltip } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { deleteActionFromDb } from './addActionToDb';
import PropTypes from 'prop-types'


const actionKindsArr = [
    "question_open",
    "instruction",
    "question_close",
    "question_range",
    "stroop_game"
]

const actionLabelName = {
    "question_open": "open question",
    "instruction": "instruction",
    "question_close": "close question",
    "question_range": "range question",
    "stroop_game": "stroop game",

}

function SelectActionShow(props) {
    const [cardActionList, setcardActionList] = useState([]);
    const [selectedKind, setselectedKind] = useState();

    useEffect(() => {
        const initialCardActionList = props.action_arr.map(action => {
            return {
                key: action.action_id,
                action_name: action.action_name,
                initialValue: action.action_value
            }
        })
        if(props.action_arr == null){
            initialCardActionList = [];
        }
        setcardActionList(initialCardActionList);
    }, [])

    const delActionCard = (id) => {
        deleteActionFromDb(id,props.expKey)
        setcardActionList(cardActionList.filter(x => x.key !== id));
    }
    return (
        <div>
            <Row>
                <Select
                    placeholder="Select a Kind Of Action"
                    style={{ width: "150px" }}
                    onChange={(value) => { setselectedKind(value) }}
                >
                    {actionKindsArr.map(action_name => (
                        <Select.Option value={action_name} key={action_name}>{actionLabelName[action_name]}</Select.Option>
                    ))}
                </Select>
                <Tooltip title="add new measure">
                    <Button icon={<PlusOutlined />}
                        onClick={() => {
                            if (selectedKind == null) return;
                            setcardActionList([{
                                key: uuid(),
                                action_name: selectedKind
                            }
                                , ...cardActionList])
                        }}>
                    </Button>
                </Tooltip>
            </Row>
            {cardActionList.map(cardAction => (
                <div style={{margin: 4}}>
                <ModalOneAction 
                    action_name = {cardAction.action_name}
                    initialValue = {cardAction.initialValue}
                    key = {cardAction.key}
                    id = {cardAction.key}
                    flow_key = {props.flow_key}
                    expKey = {props.expKey}
                    delActionCard = {delActionCard.bind(this, cardAction.key)}
                />
                </div>
            ))}

        </div>
    )
}


SelectActionShow.propTypes = {
    expKey: PropTypes.string,
    flow_key: PropTypes.string,
    action_arr: PropTypes.array
}

export default SelectActionShow
