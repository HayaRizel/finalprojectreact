import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { Form, Input, Button, Tooltip, Row, Checkbox, Space, InputNumber } from 'antd'
import { useFormik } from 'formik'
import addActionToDb from '../addActionToDb'

function RangeQuestion(props) {
    const formik = useFormik({
        initialValues: {
            question_text: "",
            save_answer_option: false
        },
        onSubmit: (values) => {
            //get
            const instruction_obj = {
                "action_name": "question_range",
                "action_id": props.id,
                "action_value": {
                    "question_text": values.question_text,
                    "answer" : {
                        "min": values.min,
                        "max": values.max,
                        "step": values.step
                    }
                }
            }
            addActionToDb(instruction_obj, props.flow_key, props.expKey)
        }
    });

    useEffect(() => {
        const value = props.initialValue;
        if (value == null) return;

        formik.setFieldValue("question_text", value.question_text);
        formik.setFieldValue("min", value.answer.min);
        formik.setFieldValue("max", value.answer.max);
        formik.setFieldValue("step", value.answer.step);

    }, [])


    useEffect(() => {
        if (props.editstatus === false) {
            formik.handleSubmit();
        }
    }, [props.editstatus])


    return (
        <Form onFinish={formik.handleSubmit}>
            <Row>
                <Form.Item
                    label="write your question">
                    <Input.TextArea
                        id="question_text"
                        onChange={formik.handleChange}
                        value={formik.values.question_text}
                        rows={4}
                        disabled={!props.editstatus} />
                </Form.Item>
            </Row>
            <Row>
                <Form.Item
                    label="min number of answer">
                    <InputNumber
                        onChange={(value) => { formik.setFieldValue("min", value) }}
                        value={formik.values.min}
                        disabled={!props.editstatus} />
                </Form.Item>
            </Row>
            <Row>
                <Form.Item
                    label="max number of answer">
                    <InputNumber
                        onChange={(value) => { formik.setFieldValue("max", value) }}
                        value={formik.values.max}
                        disabled={!props.editstatus} />
                </Form.Item>
            </Row>
            <Row>
                <Form.Item
                    label="step number of answer">
                    <InputNumber
                        onChange={(value) => { formik.setFieldValue("step", value) }}
                        value={formik.values.step}
                        disabled={!props.editstatus} />
                </Form.Item>
            </Row>
        </Form>
    )
}

export default RangeQuestion
