import React, {useEffect,useState} from 'react'
import { Tabs, Form,Input,InputNumber,Row,Col} from 'antd';
import {db_experiments} from '../../../services/firestore.config'
import SelectActionShow from './SelectActionShow';
import { v4 as uuid } from 'uuid';
import { addFlowToDb, changeFlowHowMuchSelectInDb, changeFlowNameInDb, removeFlowFromDb } from './addActionToDb';

const { TabPane } = Tabs;


function SelectFlow(props) {
    const [activeKey, setactiveKey] = useState([]);
    const [panes, setpanes] = useState([]);

    useEffect(() => {
        db_experiments.doc(props.expKey)
        .get()
        .then((doc) => {
            if (doc.exists) {
                let app_flow = doc.data().app_flow;
                if(doc.data().app_flow == null || doc.data().app_flow.filter(x => x.flow_key !== "feadback").length  === 0){
                    const newFlowObj = {
                        flow_name: "default flow", 
                        action_arr: [],
                        how_much_select: 1,
                        flow_key: uuid()  
                    }
                    if(doc.data().app_flow == null){
                        app_flow = [newFlowObj];     
                    }else{
                        app_flow = [newFlowObj,...app_flow]
                    }
                    //add app_flow to db
                    db_experiments.doc(props.expKey)
                    .update({
                        app_flow: app_flow,
                    }, { merge: true })
                    .then(ret => {
                        console.log(ret);
                    })
                    .catch(err => {
                        console.error(err)
                    });            
                }
                let i = 1;
                const initialPanes = app_flow.map(flow => {
                    return {
                        flow_name: flow.flow_name, 
                        action_arr: flow.action_arr,
                        how_much_select: flow.how_much_select,
                        flow_key: flow.flow_key,
                    }
                });
                const initialPanesFilter = initialPanes.filter(x => x.flow_key !== "feadback");
                setpanes(initialPanesFilter);
                if(activeKey.length === 0){
                    setactiveKey(initialPanesFilter[0].flow_key);
                }
                return function cleanup() {
                }
            }
        })
        .catch((error) => {
            console.log("Error getting document:", error);
        });
    
    }, [panes])

  
    const onChange = activeKey1 => {
        setactiveKey(activeKey1);
    };
  
    const onEdit = (targetKey, action) => {
        if(action === "add"){
            add();
        }
        if(action === "remove"){
            remove(targetKey);
        }
    };
  
    const add = () => {
      const activeKey = uuid();
      const newFlowObj = {
        flow_name: 'New Flow', 
        action_arr: [],
        how_much_select: 1,
        flow_key: activeKey
    }
    addFlowToDb(newFlowObj, props.expKey);
    setpanes([...panes,newFlowObj]);
    setactiveKey(activeKey);
    };
  
    const remove = targetKey => {
        removeFlowFromDb(targetKey,props.expKey);
        const newPanes = panes.filter(pane => pane.flow_key !== targetKey);
        setpanes(newPanes);
        if(activeKey === targetKey){
            setactiveKey(panes[0].flow_key);
        }
      }
      const changeFlowName = (pane,changeFlowNameInDb,e) => {
          const newName = e.target.value;
        changeFlowNameInDb(pane,props.expKey, newName)
            pane.flow_name = newName;
      }
      const changeFlowHowMuchSelect = (pane,changeFlowHowMuchSelectInDb,newVal) => {
        changeFlowHowMuchSelectInDb(pane,props.expKey, newVal)
          pane.how_much_select = newVal;
    }

  
      return (
        <Tabs
          type="editable-card"
          onChange={onChange}
          activeKey={activeKey}
          onEdit={onEdit}
        >
          {panes.map(pane => (
            <TabPane tab={pane.flow_name} key={pane.flow_key} closable={pane.closable}>
                <Form>
                    <Row>
                <Form.Item
                    label="Flowname"
                    name="flow_name"
                >
                    <Input defaultValue ={pane.flow_name} value={pane.flow_name} onChange={changeFlowName.bind(this, pane,changeFlowNameInDb)}/>
                </Form.Item>
                <Form.Item
                    label="How much action to select every time"
                    name="how_much_select"
                    style={{marginLeft: 10}}
                >
                    <InputNumber defaultValue={pane.how_much_select} value={pane.how_much_select} onChange={changeFlowHowMuchSelect.bind(this, pane,changeFlowHowMuchSelectInDb)}/>
                </Form.Item>
                </Row>

                </Form>
                <SelectActionShow
                key = {pane.flow_key}
                flow_key = {pane.flow_key}
                expKey = {props.expKey}
                action_arr = {pane.action_arr}
                />
            </TabPane>
          ))}
        </Tabs>
      );
    
  }


export default SelectFlow

