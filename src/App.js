import React, {useEffect,useState} from 'react'
import './App.css';
import CreateAppDetails from './component/createAppDetails/CreateAppDetails';
import Instruction from './component/createAppDetails/selectActionShow/actionKinds/Instruction';
import CloseQuestion from './component/createAppDetails/selectActionShow/actionKinds/CloseQuestion';
import ModalOneAction from './component/createAppDetails/selectActionShow/ModalOneAction';
import SelectActionShow from './component/createAppDetails/selectActionShow/SelectActionShow';
import { v4 as uuid } from 'uuid';
import ConnectWindow from './component/generalWindow/ConnectWindow';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import MainUserWindow from './component/generalWindow/mainUserWindow/MainUserWindow';
import CreateNewExperiment from './component/createAppDetails/newExperiment/CreateNewExperiment';
import UserNavBar from './component/generalWindow/UserNavBar';
import MainShowDataWindow from './component/ShowData/MainShowDataWindow';
import Signup from './component/generalWindow/Signup';
import "firebase/auth";
import "firebase/firestore"
import firebase from 'firebase/app'

function App() {
  const [userId, setuserId] = useState(null);
  const [finishLoading, setstartLoading] = useState(false);
  const changeUserIdAfterConnect = (newUserId) => {
    setuserId('userId', newUserId);
  }


  useEffect(() => {
      firebase.auth().onAuthStateChanged((authUser) => {
        if (authUser) {
          setuserId(authUser.uid);
          setstartLoading(true);
        } else {
          setuserId(null);
          setstartLoading(true);
        }
      });
  }, []);

  return (<div className="App" >
    {finishLoading &&
    <Router>
      <div>
        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
          
        {userId != null ?
          <Switch>
          <Route path="/editExperiment/:experimentKey">
            <UserNavBar/>
            <CreateAppDetails userId = {userId}/>
          </Route>
        <Route path="/addExperiment/:experimentKey">
        <UserNavBar/>
            <CreateNewExperiment userId = {userId}/>
          </Route>

          <Route path="/researchData/:experimentKey">
            <UserNavBar/>
            <MainShowDataWindow userId = {userId}/>
          </Route>

          <Route path="/">
          <UserNavBar/>
            <MainUserWindow userId = {userId}/>
          </Route>
          </Switch>
          :
        <Switch>

          <Route path="/accounts/signup">
            <Signup onFinish = {changeUserIdAfterConnect}/>
          </Route>
          <Route path="/">
            <ConnectWindow onFinish = {changeUserIdAfterConnect}/>
          </Route>
          <Route render={() => <div>Not Found</div>} />
        </Switch>}
      </div>
    </Router>}

  </div>
  );
}

export default App;